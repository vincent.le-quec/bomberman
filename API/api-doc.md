# Bomberman API doc

> for each of those routes, you need to add your token :

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| token | String | your token |

##### Status code
> 401 : not authorized

------

## Users
#### GET  get every users information
- `/users`

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the user id |
| login | String | the user email |
| name | String | the user name |

##### Status code
> 200 : succeed

> 500 : internat server error

------
#### GET  get user information for id
- `/users?id=XXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| id | int | the user id |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the user id |
| login | String | the user email |
| name | String | the user name |

##### Status code
> 200 : succeed

> 204 : no user for this id

> 500 : internat server error

------
#### GET is login free
- `/users?login=XXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| login | String | the user login |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| result | boolean | true if login is free |

##### Status code
> 200 : succeed

> 500 : internat server error

------
#### GET is connection authorized
- `/users?login=XXX&password=XXXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| login | String | the user login |
| password | String | the user password SHA256 hashed |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| result | boolean | is connection authorized |
| id | String | the user id for this login & password or null if not found |

##### Status code
> 200 : succeed

> 500 : internal server error

----
#### POST Insert a new user
- `/users?name=XXX&login=XXX&password=XXXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| name | String | the user name |
| login | String | the user login |
| password | String | the user password SHA256 hashed |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the created user id |

##### Status code
> 200 : succeed

> 400 : bad request (login might already be taken)

> 500 : internat server error

----
## Game
#### GET every games information
- `/games`

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the game id |
| room | String | the game room |
| date | String | format 'yyyy-MM-dd HH-mm-ss' |
| scores | List | list of scores |

##### list of scores
| key | type | value |
| :--- | :--- | :--- |
| user_id | String | the user id |
| name | String | the user name |
| login | String | the user email |
| score | int | the score the user made in this game |
| win | boolean | did the user win this game |

##### Status code
> 200 : succeed

> 500 : internal error

----
#### POST insert a new game
- `/games?room=XXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| room | String | the user room |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the created game id |

##### Status code
> 200 : succeed

> 500 : internal error

----
## Score
#### GET every games information
- `/scores`

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the user id |

##### Status code
> 200 : succeed

> 500 : internal error

----
#### POST insert a new score
- `/games?id_game=XXX&id_user=XXX&score=XXX&win=XXX`

##### Parameter
| key | type | value |
| :--- | :--- | :--- |
| id_game | int | the game id |
| id_user | int | the user id |
| score | int | the score of the user |
| win | boolean | did the user win this game |

##### Response
| key | type | value |
| :--- | :--- | :--- |
| id | String | the created score id |

##### Status code
> 200 : succeed

> 500 : internal error