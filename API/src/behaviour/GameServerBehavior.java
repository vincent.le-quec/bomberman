package behaviour;

import behaviour.model.GameBehavior;
import behaviour.model.ScoreBehavior;
import behaviour.model.UserBehavior;
import main.StatusCode;
import server.Response;

public class GameServerBehavior extends Behaviour {

	public GameServerBehavior() {}
	
	/* ========== USERS ========= */
	@Override
	public Response getUser(String id, String login, String password) {
		// get all users
		if(id == null && login == null && password == null) {
			return getUsers();
			
		// get one user
		} else if(id != null && login == null && password == null) {
			return getUserForId(id);
			
		// is login free
		} else if(id == null && login != null && password == null) {
			return isLoginFree(login);
		
		// is connection authorized for login/password
		} else if(id == null && login != null && password != null) {
			return isConnectionAuthorized(login, password);
		}
			
		return new Response(StatusCode.BAD_REQUEST, "");
	}

	@Override
	public Response getUsers() {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response getUserForId(String id) {
		return new UserBehavior().getUserForId(id);
	}
	
	@Override
	public Response updateUser(String id, String name, String login, String password) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}
	
	@Override
	public Response deleteUser(int id) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response isLoginFree(String login) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response isConnectionAuthorized(String login, String password) {
		return new UserBehavior().isConnectionAuthorized(login, password);
	}

	@Override
	public Response addUser(String name, String login, String password) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	/* ========== GAMES ========= */
	@Override
	public Response getGames() {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response addGame(String room) {
		return new GameBehavior().addGame(room);
	}

	/* ========== SCORES ========= */
	@Override
	public Response getScores() {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response getScoresForUser(int id) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response getScoresForGame(int id) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	@Override
	public Response addScore(int gameId, int userId, int score, boolean win) {
		return new ScoreBehavior().addScore(gameId, userId, score, win);
	}
}
