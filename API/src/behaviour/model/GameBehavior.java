package behaviour.model;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.dao.GameDAO;
import db.model.Game;
import exception.DBAccessException;
import main.StatusCode;
import server.Response;

public class GameBehavior {

	public GameBehavior() {} 
	
	public Response getGames() {
		List<db.entity.Game> games;
		try {
			games = new GameDAO()
					.getAll()
					.stream()
					.map(g -> new db.entity.Game(g))
					.collect(Collectors.toList());
		} catch (DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String message = "";
		
		try {
			message = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(games);
			//message = objectMapper.writeValueAsString(games);
		} catch (JsonProcessingException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		return new Response(StatusCode.SUCCESS, message);
	}
	
	public Response addGame(String room) {
		
		// set up user data
		Game game = new Game()
				.setRoom(room)
				.setDate(new Date(new java.util.Date().getTime()));
		
		// insert user
		int id;
		try {
			id = new GameDAO().insert(game);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		return new Response(StatusCode.CREATED, "{\"id\":" + id + "}");
	}
}
