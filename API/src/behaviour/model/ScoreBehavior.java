package behaviour.model;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.dao.GameDAO;
import db.dao.ScoreDAO;
import db.dao.UserDAO;
import db.model.Game;
import db.model.Score;
import db.model.User;
import exception.DBAccessException;
import main.StatusCode;
import server.Response;

public class ScoreBehavior {

	public Response getScores() {
		List<db.entity.Score> scores;
		try {
			scores = new ScoreDAO()
					.getAll()
					.stream()
					.map(s -> new db.entity.Score(s))
					.collect(Collectors.toList());
		} catch (DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String message = "";
		
		try {
			message = objectMapper.writeValueAsString(scores);
		} catch (JsonProcessingException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		return new Response(StatusCode.SUCCESS, message);
	}
	
	public Response getScoresForUser(int id) {
		return null;
	}
	
	public Response getScoresForGame(int id) {
		return null;
	}
	
	public Response addScore(int gameId, int userId, int score, boolean win) {
		
		Optional<User> user = null;
		try {
			user = new UserDAO().get(userId);
			
			if(!user.isPresent())
				return new Response(StatusCode.NOT_FOUND, "Unknown user for id=" + userId);
		} catch(DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		Optional<Game> game = null;
		try {
			game = new GameDAO().get(gameId);
			
			if(!game.isPresent())
				return new Response(StatusCode.NOT_FOUND, "Unknown game for id=" + gameId);
		} catch(DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		User myUser = user.get();
		Game myGame = game.get();

		Score myScore = new Score()
				.setUser(myUser)
				.setGame(myGame)
				.setScore(score)
				.setWin(win);
		
		// insert user
		int id;
		try {
			id = new ScoreDAO().insert(myScore);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		return new Response(StatusCode.CREATED, "{\"id\":" + id + "}");
	}
}
