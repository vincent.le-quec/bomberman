package behaviour.model;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.dao.UserDAO;
import db.model.User;
import exception.DBAccessException;
import main.StatusCode;
import server.Response;

public class UserBehavior {

	public UserBehavior() {}
	
	public Response getUsers() {
		List<db.entity.User> users;
		try {
			users = new UserDAO()
					.getAll()
					.stream()
					.map(u -> new db.entity.User(u))
					.collect(Collectors.toList());
		} catch (DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String message = "";
		
		try {
			message = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(users);
			//message = objectMapper.writeValueAsString(users);
		} catch (JsonProcessingException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		return new Response(StatusCode.SUCCESS, message);
	}

	public Response getUserForId(String id) {
		Optional<User> user = null;
		
		try {
			user = new UserDAO().get(Integer.parseInt(id));
		} catch (NumberFormatException | DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String message = "";
		
		if(user.isPresent()) {
			db.entity.User myUser = new db.entity.User(user.get());
			
			try {
				message = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(myUser);
				//message = objectMapper.writeValueAsString(myUser);
			} catch (JsonProcessingException e) {
				return new Response(StatusCode.INTERNAL_ERROR, "");
			}
			
		} else {
			return new Response(StatusCode.NOT_FOUND, "Unknown user with id=" + id);
		}
		
		return new Response(StatusCode.SUCCESS, message);
	}

	public Response isLoginFree(String login) {
		User user;
		
		try {
			user = new UserDAO().loginIsTaken(login);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		String message = "";
		if(user == null)
			message = "{\"result\" : false }";
		else
			message = "{\"result\" : true }";
		
		return new Response(StatusCode.SUCCESS, message);
	}

	public Response isConnectionAuthorized(String login, String password) {
		
		User user;
		
		try {
			user = new UserDAO().loginPasswordCorrect(login, password);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		String message = "";
		if(user == null)
			message = "{\"result\" : false }";
		else
			message = "{\"result\" : true, \"id\":" + user.getId() + "}";

		return new Response(StatusCode.SUCCESS, message);
	}

	public Response addUser(String name, String login, String password) {
		
		// check if login is free
		User user;
		
		try {
			user = new UserDAO().loginIsTaken(login);
			
			if(user != null)
				return new Response(StatusCode.BAD_REQUEST, "Login is already taken");
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		// set up user data
		user = new User()
				.setName(name)
				.setLogin(login)
				.setPassword(password);
		
		// insert user
		int id;
		try {
			id = new UserDAO().insert(user);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		return new Response(StatusCode.CREATED, "{\"id\":" + id + "}");
	}

	public Response updateUser(String id, String name, String login, String password) {
		
		// get user
		Optional<User> user = null;
		
		try {
			user = new UserDAO().get(Integer.parseInt(id));
		} catch (NumberFormatException | DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		// unknown user for id
		if(!user.isPresent()) {
			return new Response(StatusCode.NOT_FOUND, "{\"result\" : false, \"message\" : \"Unknown user with id=" + id + "\" }");
		}
		
		// set up user data
		User myUser = user.get()
				.setName(name)
				.setLogin(login)
				.setPassword(password);
		
		// update user
		try {
			new UserDAO().update(myUser);
		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		return new Response(StatusCode.UPDATED, "{\"result\" : true }");
	}

	public Response deleteUser(int id) {
		
		// get user
		Optional<User> user = null;
		
		try {
			user = new UserDAO().get(id);
		} catch (NumberFormatException | DBAccessException e1) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}
		
		// unknown user for id
		if(!user.isPresent()) {
			return new Response(StatusCode.NOT_FOUND, "{\"result\" : false, \"message\" : \"Unknown user with id=" + id + "\" }");
		}
		
		// delete user
		try {
			
			new UserDAO().delete(user.get());

		} catch (DBAccessException e) {
			return new Response(StatusCode.INTERNAL_ERROR, "");
		}

		return new Response(StatusCode.DELETED, "{\"result\" : true }");
	}
}
