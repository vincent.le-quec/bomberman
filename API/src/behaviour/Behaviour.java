package behaviour;

import exception.WrongTokenException;
import server.Response;

public abstract class Behaviour {

	private static final String WEB_CLIENT_TOKEN = "FpbjSwXy4xOQbP7IGjrCGf75e6FAKGIW9CjhxYrpJwBcFHeLZVjJPIZQNumh2M1r";
	private static final String GAME_SERVER_TOKEN = "2HslE8EiMXYDhMBJeJ1mSVdStpnkfLqqpkyze5uq4Nk5eGdpJAbPMcyfFWm5RVVr";

	public static Behaviour getBehavior(String token) throws WrongTokenException {
		
		if(WEB_CLIENT_TOKEN.equals(token))
			return new WebClientBehavior();
		else if (GAME_SERVER_TOKEN.equals(token))
			return new GameServerBehavior();
		
		throw new WrongTokenException("Unknown token=" + token);
	}
	
	public abstract Response getUser(String id, String login, String password);
	public abstract Response getUsers();
	public abstract Response getUserForId(String id);
	public abstract Response updateUser(String id, String name, String login, String password);
	public abstract Response deleteUser(int id);
	
	public abstract Response isLoginFree(String login);
	public abstract Response isConnectionAuthorized(String login, String password);
	public abstract Response addUser(String name, String login, String password);
	
	
	public abstract Response getGames();
	public abstract Response addGame(String room);
	
	
	public abstract Response getScores();
	public abstract Response getScoresForUser(int id);
	public abstract Response getScoresForGame(int id);
	public abstract Response addScore(int idGame, int idUser, int score, boolean win);
}
