package behaviour;

import behaviour.model.GameBehavior;
import behaviour.model.ScoreBehavior;
import behaviour.model.UserBehavior;
import main.StatusCode;
import server.Response;

public class WebClientBehavior extends Behaviour {
	
	public WebClientBehavior() {}

	/* ========== USERS ========= */
	@Override
	public Response getUser(String id, String login, String password) {
		System.out.println("WebClientBehaviour::getUser::id=" + id + ", login=" + login + ", password=" + password);
		// get all users
		if(id == null && login == null && password == null) {
			return getUsers();
			
		// get one user
		} else if(id != null && login == null && password == null) {
			return getUserForId(id);
			
		// is login free
		} else if(id == null && login != null && password == null) {
			return isLoginFree(login);
		
		// is connection authorized for login/password
		} else if(id == null && login != null && password != null) {
			return isConnectionAuthorized(login, password);
		}
		
		System.out.println("WebClientBehavior::bad request");
		return new Response(StatusCode.BAD_REQUEST, "");
	}

	@Override
	public Response getUsers() {
		return new UserBehavior().getUsers();
	}

	@Override
	public Response getUserForId(String id) {
		return new UserBehavior().getUserForId(id);
	}
	
	@Override
	public Response updateUser(String id, String name, String login, String password) {
		return new UserBehavior().updateUser(id, name, login, password);
	}
	
	@Override
	public Response deleteUser(int id) {
		return new UserBehavior().deleteUser(id);
	}

	@Override
	public Response isLoginFree(String login) {
		return new UserBehavior().isLoginFree(login);
	}

	@Override
	public Response isConnectionAuthorized(String login, String password) {
		return new UserBehavior().isConnectionAuthorized(login, password);
	}

	@Override
	public Response addUser(String name, String login, String password) {
		return new UserBehavior().addUser(name, login, password);
	}

	/* ========== GAMES ========= */
	@Override
	public Response getGames() {
		return new GameBehavior().getGames();
	}

	@Override
	public Response addGame(String room) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}

	/* ========== SCORES ========= */
	@Override
	public Response getScores() {
		return new ScoreBehavior().getScores();
	}

	@Override
	public Response getScoresForUser(int id) {
		return new ScoreBehavior().getScoresForUser(id);
	}

	@Override
	public Response getScoresForGame(int id) {
		return new ScoreBehavior().getScoresForGame(id);
	}

	@Override
	public Response addScore(int userId, int gameId, int score, boolean win) {
		return new Response(StatusCode.REQUEST_NOT_AUTHORIZED, "Unauthorized request !");
	}
}
