--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 9.5.19

-- Started on 2020-01-30 09:47:28 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE bomberman;
--
-- TOC entry 2180 (class 1262 OID 16384)
-- Name: bomberman; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE bomberman WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';


ALTER DATABASE bomberman OWNER TO postgres;

\connect bomberman

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12403)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 16761)
-- Name: game; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.game (
    id integer NOT NULL,
    date date DEFAULT ('now'::text)::date NOT NULL,
    room character varying(255) NOT NULL
);


ALTER TABLE public.game OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16759)
-- Name: GAME_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."GAME_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."GAME_id_seq" OWNER TO postgres;

--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 185
-- Name: GAME_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."GAME_id_seq" OWNED BY public.game.id;


--
-- TOC entry 184 (class 1259 OID 16752)
-- Name: score; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.score (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_game integer NOT NULL,
    win boolean DEFAULT false NOT NULL,
    score integer NOT NULL
);


ALTER TABLE public.score OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16750)
-- Name: SCORE_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SCORE_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SCORE_id_seq" OWNER TO postgres;

--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 183
-- Name: SCORE_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SCORE_id_seq" OWNED BY public.score.id;


--
-- TOC entry 182 (class 1259 OID 16739)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16737)
-- Name: USER_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."USER_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."USER_id_seq" OWNER TO postgres;

--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 181
-- Name: USER_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."USER_id_seq" OWNED BY public."user".id;


--
-- TOC entry 2043 (class 2604 OID 16764)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game ALTER COLUMN id SET DEFAULT nextval('public."GAME_id_seq"'::regclass);


--
-- TOC entry 2041 (class 2604 OID 16755)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.score ALTER COLUMN id SET DEFAULT nextval('public."SCORE_id_seq"'::regclass);


--
-- TOC entry 2040 (class 2604 OID 16742)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public."USER_id_seq"'::regclass);


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 185
-- Name: GAME_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."GAME_id_seq"', 2, true);


--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 183
-- Name: SCORE_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SCORE_id_seq"', 8, true);


--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 181
-- Name: USER_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."USER_id_seq"', 3, true);


--
-- TOC entry 2174 (class 0 OID 16761)
-- Dependencies: 186
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.game (id, date, room) VALUES (1, '2020-01-01', 'room1');
INSERT INTO public.game (id, date, room) VALUES (2, '2020-01-10', 'room2');


--
-- TOC entry 2172 (class 0 OID 16752)
-- Dependencies: 184
-- Data for Name: score; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.score (id, id_user, id_game, win, score) VALUES (1, 1, 1, false, 50);
INSERT INTO public.score (id, id_user, id_game, win, score) VALUES (2, 2, 1, true, 100);
INSERT INTO public.score (id, id_user, id_game, win, score) VALUES (3, 1, 2, false, 50);
INSERT INTO public.score (id, id_user, id_game, win, score) VALUES (4, 3, 2, true, 100);


--
-- TOC entry 2170 (class 0 OID 16739)
-- Dependencies: 182
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" (id, name, login, password) VALUES (1, 'pierre', 'pierre login', 'pierre email');
INSERT INTO public."user" (id, name, login, password) VALUES (2, 'paul', 'paul login', 'paul email');
INSERT INTO public."user" (id, name, login, password) VALUES (3, 'jacques', 'jacques login', 'jacques email');


--
-- TOC entry 2052 (class 2606 OID 16767)
-- Name: GAME_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "GAME_pk" PRIMARY KEY (id);


--
-- TOC entry 2050 (class 2606 OID 16758)
-- Name: SCORE_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.score
    ADD CONSTRAINT "SCORE_pk" PRIMARY KEY (id);


--
-- TOC entry 2046 (class 2606 OID 16749)
-- Name: USER_login_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "USER_login_key" UNIQUE (login);


--
-- TOC entry 2048 (class 2606 OID 16747)
-- Name: USER_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "USER_pk" PRIMARY KEY (id);


--
-- TOC entry 2053 (class 2606 OID 16768)
-- Name: SCORE_fk0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.score
    ADD CONSTRAINT "SCORE_fk0" FOREIGN KEY (id_user) REFERENCES public."user"(id);


--
-- TOC entry 2054 (class 2606 OID 16773)
-- Name: SCORE_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.score
    ADD CONSTRAINT "SCORE_fk1" FOREIGN KEY (id_game) REFERENCES public.game(id);


--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-01-30 09:47:28 CET

--
-- PostgreSQL database dump complete
--

