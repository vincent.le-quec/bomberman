
CREATE TABLE "USER" (
	"id" serial NOT NULL,
	"name" varchar(255) NOT NULL,
	"login" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL,
	CONSTRAINT "USER_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "SCORE" (
	"id" serial NOT NULL,
	"id_user" integer NOT NULL,
	"id_game" integer NOT NULL,
	"win" BOOLEAN NOT NULL DEFAULT 'false',
	"score" integer NOT NULL,
	CONSTRAINT "SCORE_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "GAME" (
	"id" serial NOT NULL,
	"date" DATE NOT NULL DEFAULT CURRENT_DATE,
	"room" varchar(255) NOT NULL,
	CONSTRAINT "GAME_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

ALTER TABLE "SCORE" ADD CONSTRAINT "SCORE_fk0" FOREIGN KEY ("id_user") REFERENCES "USER"("id");
ALTER TABLE "SCORE" ADD CONSTRAINT "SCORE_fk1" FOREIGN KEY ("id_game") REFERENCES "GAME"("id");

INSERT INTO "USER"("name", "login", "password") VALUES
('pierre','pierre login','pierre passwd'),
('paul','paul login','paul passwd'),
('jacques','jacques login','jacques passwd');

INSERT INTO "GAME"("date", "room") VALUES
('2020-01-01','room1'),
('2020-01-10','room2');

INSERT INTO public."SCORE"(id_user, id_game, win, score) VALUES
(1, 1, FALSE, 50),
(2, 1, TRUE, 100),
(1, 2, FALSE, 50),
(3, 2, TRUE, 100);

