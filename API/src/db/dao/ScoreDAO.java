package db.dao;

import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;

import db.HibernateUtil;
import db.model.Score;
import exception.DBAccessException;

public class ScoreDAO implements DAO<Score> {

	@Override
	public Optional<Score> get(int id) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            Optional<Score> user = Optional.ofNullable(session.get(Score.class, id));

            return user;
            
        } catch (Exception e) {
        	throw new DBAccessException("");
        }
	}

	@Override
	public List<Score> getAll() throws DBAccessException {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from Score", Score.class).getResultList();
        } catch(Exception e) {
        	throw new DBAccessException("");
        }
	}

	public List<Score> getForUser(int id) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
        	return session.createQuery("from Score where id_user='" + id + "'", Score.class).getResultList();
            
        } catch (Exception e) {
            throw new DBAccessException("");
        }
	}

	@Override
	public int insert(Score o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            
            // start a transaction
            transaction = session.beginTransaction();
            int id = (int) session.save(o);
            session.persist(o);
            transaction.commit();

            return id;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean update(Score o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            session.update(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean delete(Score o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            session.delete(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}
}
