package db.dao;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;

import db.HibernateUtil;
import db.model.Score;
import db.model.User;
import exception.DBAccessException;

public class UserDAO implements DAO<User> {

	@Override
	public Optional<User> get(int id) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            Optional<User> user = Optional.ofNullable(session.get(User.class, id));

            return user;
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new DBAccessException("");
        }
	}

	public User loginIsTaken(String login) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

			List<User> users = session.createQuery("from User where login='" + login + "'", User.class).getResultList();

            System.out.println("User=" + users.stream().map(User::toString).collect(Collectors.joining(", ")));
            if(users.size() > 0) {
            	return users.get(0);
            }
            
        } catch (Exception e) {
            throw new DBAccessException("");
        }
        
        return null;
	}

	public User loginPasswordCorrect(String login, String password) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

			List<User> users = session.createQuery("from User where login='" + login + "' and password='" + password + "'", User.class).getResultList();

            if(users.size() > 0) {
            	return users.get(0);
            }
            
        } catch (Exception e) {
            throw new DBAccessException("");
        }
        
        return null;
	}

	@Override
	public List<User> getAll() throws DBAccessException {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from User", User.class).getResultList();
        }
	}

	@Override
	public int insert(User o) throws DBAccessException {
		
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            int id = (int) session.save(o);
            session.persist(o);
            transaction.commit();
            session.close();

            return id;
            
        } catch (Exception e) {
        	if(transaction != null)
        		transaction.rollback();
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean update(User o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            session.update(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean delete(User o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
        	List<Score> scores = new ScoreDAO().getForUser(o.getId());
        	
            // start a transaction
            transaction = session.beginTransaction();
            scores.forEach(s -> session.delete(s));
            session.delete(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            throw new DBAccessException("");
        }
	}
}
