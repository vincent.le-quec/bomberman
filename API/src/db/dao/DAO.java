package db.dao;

import java.util.List;
import java.util.Optional;

import exception.DBAccessException;

public interface DAO<T> {
	
	Optional<T> get(int id) throws DBAccessException;
    List<T> getAll() throws DBAccessException;
	int insert(T o) throws DBAccessException;
	boolean update(T o) throws DBAccessException;
	boolean delete(T o) throws DBAccessException;
}
