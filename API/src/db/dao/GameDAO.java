package db.dao;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;

import db.HibernateUtil;
import db.model.Game;
import exception.DBAccessException;

public class GameDAO implements DAO<Game> {

	@Override
	public Optional<Game> get(int id) throws DBAccessException {
		
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            Optional<Game> user = Optional.ofNullable(session.get(Game.class, id));

            return user;
            
        } catch (Exception e) {
        	throw new DBAccessException("");
        }
	}

	@Override
	public List<Game> getAll() throws DBAccessException {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {			
            return session.createQuery("from Game", Game.class).getResultList()
            		.stream()
            		.sorted(Comparator.comparing(Game::getDate).reversed())
            		.collect(Collectors.toList());
        } catch(Exception e) {
        	throw new DBAccessException("");
        }
	}

	@Override
	public int insert(Game o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            int id = (int) session.save(o);
            session.persist(o);
            transaction.commit();

            return id;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean update(Game o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            session.update(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

	@Override
	public boolean delete(Game o) throws DBAccessException {
		
		Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
        	
            // start a transaction
            transaction = session.beginTransaction();
            session.delete(o);
            transaction.commit();

            return true;
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DBAccessException("");
        }
	}

}
