package db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "score", schema = "public")
public class Score {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	
	@Column(name = "win")
	boolean win;
	
	@Column(name = "score")
	int score;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_user")
	User user;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_game")
	Game game;
	
	public Score() {}

	public int getId() {
		return id;
	}

	public Score setId(int id) {
		this.id = id;
		return this;
	}

	public boolean isWin() {
		return win;
	}

	public Score setWin(boolean win) {
		this.win = win;
		return this;
	}

	public int getScore() {
		return score;
	}

	public Score setScore(int score) {
		this.score = score;
		return this;
	}

	public User getUser() {
		return user;
	}

	public Score setUser(User user) {
		this.user = user;
		return this;
	}

	public Game getGame() {
		return game;
	}

	public Score setGame(Game game) {
		this.game = game;
		return this;
	}

	@Override
	public String toString() {
		return "Score [id=" + id + ", win=" + win + ", score=" + score + "]";
	}
}
