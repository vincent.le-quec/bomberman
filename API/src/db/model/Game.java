package db.model;

import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "game", schema = "public")
public class Game {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	
	@Column(name = "date")
	Date date;
	
	@Column(name = "room")
	String room;
	
	@OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    private List<Score> scores ;
	
	public Game() {}

	public int getId() {
		return id;
	}

	public Game setId(int id) {
		this.id = id;
		return this;
	}

	public Date getDate() {
		return date;
	}

	public Game setDate(Date date) {
		this.date = date;
		return this;
	}

	public String getRoom() {
		return room;
	}

	public Game setRoom(String room) {
		this.room = room;
		return this;
	}

	public Collection<Score> getScores() {
		return scores;
	}

	public Game setScores(List<Score> scores) {
		this.scores = scores;
		return this;
	}

	@Override
	public String toString() {
		return "Game [id=" + id + ", date=" + date + ", room=" + room + ", scores=[" + scores.stream().map(s -> s.toString()).collect(Collectors.joining(", ")) + "]]";
	}
}
