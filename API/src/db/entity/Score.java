package db.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Score {

	@JsonProperty("id")
	int id;
	@JsonProperty("win")
	boolean win;
	@JsonProperty("score")
	int score;
	@JsonProperty("user_id")
	int user_id;
	@JsonProperty("login")
	String login;
	@JsonProperty("name")
	String name;
	@JsonProperty("game_id")
	int game_id;
	@JsonProperty("date")
	String date;
	@JsonProperty("room")
	String room;
	
	public Score() {}

	public Score(db.model.Score score) {
		this.id = score.getId();
		this.win = score.isWin();
		this.score = score.getScore();
		this.game_id = score.getGame().getId();
		this.user_id = score.getUser().getId();
		this.login = score.getUser().getLogin();
		this.name = score.getUser().getName();
		this.date = String.valueOf(score.getGame().getDate().getTime());
		this.room = score.getGame().getRoom();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGame_id() {
		return game_id;
	}

	public void setGame_id(int game_id) {
		this.game_id = game_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "Score [id=" + id + ", win=" + win + ", score=" + score + ", user_id=" + user_id + ", login=" + login
				+ ", name=" + name + ", game_id=" + game_id + ", date=" + date + ", room=" + room + "]";
	}
}
