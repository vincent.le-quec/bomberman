package db.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	@JsonProperty("id")
	int id;
	@JsonProperty("login")
	String login;
	@JsonProperty("name")
	String name;
	@JsonProperty("scores")
	List<Score> scores = new ArrayList<>();
	
	public User() {}
	
	public User(db.model.User user) {
		System.out.println("hey there !");
		this.id = user.getId();
		this.login = user.getLogin();
		this.name = user.getName();
		this.scores = user.getScores()
				.stream()
				.map(s -> new Score(s))
				.collect(Collectors.toList());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", name=" + name + "]";
	}
}
