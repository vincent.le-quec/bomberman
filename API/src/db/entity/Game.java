package db.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Game {

	@JsonProperty("id")
	int id;
	@JsonProperty("date")
	String date;
	@JsonProperty("room")
	String room;
	@JsonProperty("scores")
	List<Score> scores = new ArrayList<>();
	
	public Game() {}
	
	public Game(db.model.Game game) {
		this.id = game.getId();
		this.date = String.valueOf(game.getDate().getTime());
		this.room = game.getRoom();
		this.scores = game.getScores()
				.stream()
				.map(s -> new Score(s))
				.collect(Collectors.toList());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "Game [id=" + id + ", date=" + date + ", room=" + room + ", scores=" + scores + "]";
	}	
}
