package server;

import main.StatusCode;

public class Response {

	StatusCode statusCode;
	String message;
	
	public Response(StatusCode statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode.getStatusCode();
	}

	public String getMessage() {
		return message;
	}
}
