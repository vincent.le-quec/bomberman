package exception;

public class DBAccessException extends Exception {

	private static final long serialVersionUID = 1L;

	public DBAccessException(String message) {
		super(message);
	}
}
