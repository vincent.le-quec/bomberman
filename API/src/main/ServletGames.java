package main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import behaviour.Behaviour;
import exception.WrongTokenException;
import server.Response;

@WebServlet("/games")
public class ServletGames extends HttpServlet {

	//final static Logger LOGGER = Logger.getLogger(Hello.class);
	private static final long serialVersionUID = 1L;
	private static final String FIELD_ROOM = "room";

    public ServletGames() {
        super();
    }

    /* TODO trier par order décroissant de date */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter(ServletUsers.FIELD_TOKEN);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			return;
		}
		
		Response res = behavior.getGames();
		
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String token = request.getParameter(ServletUsers.FIELD_TOKEN);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			return;
		}
		
		String room = request.getParameter(FIELD_ROOM);
		
		Response res = behavior.addGame(room);
		
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}
}
