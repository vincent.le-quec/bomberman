package main;

public enum StatusCode {
	SUCCESS(200), // get one, get all
	CREATED(201), // put
	UPDATED(202), // post
	DELETED(202), // del
	NOT_FOUND(204), // get one, del
	BAD_REQUEST(400), // client bad request
	REQUEST_NOT_AUTHORIZED(401), // request not auhtorized for client
	INTERNAL_ERROR(500); // put, post, del


    private final int statusCode;

    private StatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
    
    public int getStatusCode() {
    	return statusCode;
    }
}
