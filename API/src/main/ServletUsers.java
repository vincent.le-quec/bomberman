package main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import main.StatusCode;
import server.Response;
import behaviour.Behaviour;
import exception.WrongTokenException;

@WebServlet("/users")
public class ServletUsers extends HttpServlet {
	
	//final static Logger LOGGER = Logger.getLogger(Hello.class);
	private static final long serialVersionUID = 1L;
	protected static final String FIELD_TOKEN = "token";
	private static final String FIELD_ID = "id";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_LOGIN = "login";
	private static final String FIELD_PASSWORD = "password";

    public ServletUsers() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter(FIELD_TOKEN);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			response.addHeader("Access-Control-Allow-Origin", "*");
			return;
		}
		
		String id = request.getParameter(FIELD_ID);
		String login = request.getParameter(FIELD_LOGIN);
		String password = request.getParameter(FIELD_PASSWORD);
		System.out.println("USER:GET:id=" + id + ", login=" + login + ", password=" + password);
		
		Response res = behavior.getUser(id, login, password);
		
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter(FIELD_TOKEN);
		String name = request.getParameter(FIELD_NAME);
		String login = request.getParameter(FIELD_LOGIN);
		String password = request.getParameter(FIELD_PASSWORD);
		System.out.println("USER:PUT: name=" + name + ", login=" + login + ", password=" + password + ", token=" + token);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			response.addHeader("Access-Control-Allow-Origin", "*");
			return;
		}

		Response res = behavior.addUser(name, login, password);
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter(FIELD_TOKEN);
		String id = request.getParameter(FIELD_ID);
		String name = request.getParameter(FIELD_NAME);
		String login = request.getParameter(FIELD_LOGIN);
		String password = request.getParameter(FIELD_PASSWORD);
		System.out.println("USER:POST: name=" + name + ", login=" + login + ", password=" + password + ", token=" + token);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			response.addHeader("Access-Control-Allow-Origin", "*");
			return;
		}

		Response res = behavior.updateUser(id, name, login, password);
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String token = request.getParameter(FIELD_TOKEN);
		int id = Integer.parseInt(request.getParameter(FIELD_ID));
		System.out.println("USER:DELETE: id=" + id + ", token=" + token);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			response.addHeader("Access-Control-Allow-Origin", "*");
			return;
		}

		Response res = behavior.deleteUser(id);
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}
}
