package main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import behaviour.Behaviour;
import exception.WrongTokenException;
import server.Response;

@WebServlet("/scores")
public class ServletScores extends HttpServlet {

	//final static Logger LOGGER = Logger.getLogger(Hello.class);
	private static final long serialVersionUID = 1L;
	private static final String FIELD_WIN = "win";
	private static final String FIELD_SCORE = "score";
	private static final String FIELD_ID_USER = "id_user";
	private static final String FIELD_ID_GAME = "id_game";

    public ServletScores() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String token = request.getParameter(ServletUsers.FIELD_TOKEN);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			return;
		}
		
		Response res = behavior.getScores();
		
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//LOGGER.info("just un post");
		
		String token = request.getParameter(ServletUsers.FIELD_TOKEN);
		
		Behaviour behavior = null;
		try {
			behavior = Behaviour.getBehavior(token);
		} catch(WrongTokenException e) {
			response.setStatus(StatusCode.BAD_REQUEST.getStatusCode());
			return;
		}

		boolean win = Boolean.parseBoolean(request.getParameter(FIELD_WIN));
		int score = Integer.parseInt(request.getParameter(FIELD_SCORE));
		int idUser = Integer.parseInt(request.getParameter(FIELD_ID_USER));
		int idGame = Integer.parseInt(request.getParameter(FIELD_ID_GAME));
		
		Response res = behavior.addScore(idGame, idUser, score, win);
		
		System.out.println("MY RESPONSE = " + res.getMessage());
		response.getWriter().append(res.getMessage());
		response.setStatus(res.getStatusCode());
		response.addHeader("Access-Control-Allow-Origin", "*");
	}
}
