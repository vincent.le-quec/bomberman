package Main;

import Main.Panel.View;
import Services.Service;

public class Main {
    public static void main(String[] args) {
        try {
            Service service = new Service();
            View view = new View(service);
            service.setView(view);
        } catch (Exception e) {
            System.out.println("[ERROR] Error during Bomberman render: " + e.toString());
            System.exit(1);
        }
    }
}
