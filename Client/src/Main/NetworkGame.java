package Main;

import Main.Info.AgentAction;
import Main.Panel.PanelBombermanGame;
import Services.Data.Received.DataMap;
import Services.Service;

import javax.swing.*;

public class NetworkGame extends Thread {
    private Map map;
    private Service service;
    private PanelBombermanGame panelBomberman;

    public NetworkGame(Map map, Service service) {
        this.map = map;
        this.service = service;
    }

    public void keyboardAction(AgentAction action) {
        if (!this.service.disableAction)
            if (this.service.yourTurn) {
                System.out.println("Send Action: " + action.toString());
                this.service.action(action.toString());
                this.service.disableAction = true;
                this.service.yourTurn = false;
            } else
                System.out.println("It's not your turn !");
        else
            System.out.println("The action is being processed");
    }

    @Override
    public void run() {
        super.run();
        this.panelBomberman.update(this.map);
        while (!this.service.gameOver)
            if (!this.service.yourTurn) {
                DataMap dataMap = this.service.waitMap();
                if (dataMap != null)
                    try {
                        Map map = new Map(dataMap.getMap(), dataMap.getItems(), dataMap.getBombs());
                        this.panelBomberman.update(map);
                    } catch (Exception e) {
                        System.err.println("[ERROR] Load map failed: " + e.toString());
                    }
            }
        this.interrupt();
    }

    public void setPanelBomberman(PanelBombermanGame panelBomberman) {
        this.panelBomberman = panelBomberman;
    }

    public JPanel getPanelBomberman() {
        return panelBomberman.getPanel();
    }
}
