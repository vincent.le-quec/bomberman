package Main.Info;

public enum ItemType {
    FIRE_UP,
    FIRE_DOWN,
    BOMB_UP,
    BOMB_DOWN,
    FIRE_SUIT,
    SKULL
}