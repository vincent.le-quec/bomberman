package Main;

public enum TypeGame {
    SOLO("1"),
    DUO("2");

    private String code;
    TypeGame(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
