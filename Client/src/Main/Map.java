package Main;

import Main.Info.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de charger une carte de Bomberman à partir d'un fichier de layout d'extension .lay
 *
 * @author Kevin Balavoine, Victor Lelu--Ribaimont modifié par Vincent LE QUEC
 */

public class Map implements Serializable {
    private static final long serialVersionUID = 1L;

    private int size_x;
    private int size_y;

    private boolean[][] walls;
    private boolean[][] start_brokable_walls;

    private List<InfoAgent> start_agents;
    private List<InfoItem> items;
    private List<InfoBomb> bombs;

    private int cpt_col;
    private static ColorAgent[] color = ColorAgent.values();

    public Map(String str, List<InfoItem> items, List<InfoBomb> bombs) throws Exception {
        String[] map = str.split("\n");

        int nbX = 0;
        int nbY = 0;

        for (String line : map) {
            line = line.trim();
            if (nbX == 0) {
                nbX = line.length();
            } else if (nbX != line.length())
                throw new Exception("Toutes les lignes doivent avoir la même longueur");
            nbY++;
        }
        size_x = nbX;
        size_y = nbY;

        walls = new boolean[size_x][size_y];
        start_brokable_walls = new boolean[size_x][size_y];

        int y = 0;

        this.cpt_col = 0;

        start_agents = new ArrayList<>();

        for (String line : map) {
            this.loadMapInfo(line, y);
            y++;
        }

        this.checkLabyrinth(y);

        this.items = items;
        this.bombs = bombs;
    }

    public void loadMapInfo(String line, int y) {
        line = line.trim();
        /**
         * % - mur incassable
         * $ - mur cassable
         * Agents - B: Bomberman; E: ennemi; R: rajion; V: bird
         */
        for (int x = 0; x < line.length(); x++) {

            walls[x][y] = line.charAt(x) == '%';
            start_brokable_walls[x][y] = line.charAt(x) == '$';

            switch (line.charAt(x)) {
                case 'E':
                case 'V':
                case 'R':
                    start_agents.add(new InfoAgent(x, y, AgentAction.STOP, line.charAt(x), ColorAgent.DEFAULT, false, false));
                    break;
                case 'B':
                    ColorAgent col = (cpt_col < color.length) ? color[cpt_col] : ColorAgent.DEFAULT;
                    start_agents.add(new InfoAgent(x, y, AgentAction.STOP, line.charAt(x), col, false, false));
                    cpt_col++;
                    break;
            }
        }
    }

    public boolean isAgentOnPath(int x, int y) {
        for (InfoAgent agent : this.start_agents)
            if (agent.getX() == x && agent.getY() == y)
                return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder map = new StringBuilder();

        for (int y = 0; y < this.size_y; y++) {
            for (int x = 0; x < this.size_x; x++) {
                if (this.walls[x][y])
                    map.append('%');
                else if (this.start_brokable_walls[x][y])
                    map.append('$');
                else if (isAgentOnPath(x, y)) {
                    for (InfoAgent agent : this.start_agents)
                        if (agent.getX() == x && agent.getY() == y) {
                            map.append(agent.getType());
                            break;
                        }
                } else
                    map.append(' ');
            }
            map.append("\n");
        }
        return map.toString();
    }

    public void checkLabyrinth(int y) throws Exception {
        //On verifie que le labyrinthe est clos
        for (int x = 0; x < size_x; x++)
            if (!walls[x][0]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
        for (int x = 0; x < size_x; x++)
            if (!walls[x][size_y - 1]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
        for (y = 0; y < size_y; y++)
            if (!walls[0][y]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
        for (y = 0; y < size_y; y++)
            if (!walls[size_x - 1][y]) throw new Exception("Mauvais format du fichier: la carte doit etre close");
    }

    public int getSizeX() {
        return (size_x);
    }
    public int getSizeY() {
        return (size_y);
    }
    public boolean[][] getStart_brokable_walls() {
        return start_brokable_walls;
    }
    public boolean[][] get_walls() {
        return walls;
    }
    public List<InfoAgent> getStart_agents() {
        return start_agents;
    }
    public List<InfoItem> getItems() {
        return items;
    }
    public List<InfoBomb> getBombs() {
        return bombs;
    }
}