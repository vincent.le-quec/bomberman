package Main.Panel;

import Main.TypeGame;
import Services.Service;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PanelLogin {
    private JPanel panel;
    private JLabel loginLabel, passwordLabel, error;
    private JTextField inputLogin;
    private JPasswordField inputPassword;
    private JButton btnSubmit;
    private ButtonGroup radioBtn;

    private View view;
    private Service service;

    private static String saveLogin = null;
    private static String savePassword = null;

    public PanelLogin(View view, Service service) {
        this.view = view;
        this.service = service;

        this.createView();
    }

    private void createView() {
        this.loginLabel = new JLabel();
        this.loginLabel.setText("Login :");
        this.inputLogin = new JTextField();
        if (saveLogin != null)
            this.inputLogin.setText(saveLogin);

        this.passwordLabel = new JLabel();
        this.passwordLabel.setText("Password :");
        this.inputPassword = new JPasswordField();
        if (savePassword != null)
            this.inputPassword.setText(savePassword);

        this.error = new JLabel();

        this.btnSubmit = new JButton("Submit");
        this.btnSubmit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evenement) {
                MessageDigest digest = null;
                try {
                    digest = MessageDigest.getInstance("SHA-256");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }


                String login = inputLogin.getText();
                String password = bytesToHex(digest.digest(inputPassword.getText().getBytes(StandardCharsets.UTF_8)));


                String type = radioBtn.getSelection().getActionCommand();
                service.login(login, password, type);
                String res = service.waitLogin();
                if ("true".equals(res)) {
                    saveLogin = login;
                    savePassword = inputPassword.getText();
                    error.setText("Welcome " + login + "!");
                    view.goLoader();
                } else
                    error.setText("Check yours informations");
            }
        });

        this.panel = new JPanel(new GridLayout(5, 1));
        this.panel.add(this.loginLabel);
        this.panel.add(this.inputLogin);
        this.panel.add(this.passwordLabel);
        this.panel.add(this.inputPassword);

        this.createRadioBtn();

        this.panel.add(this.error);
        this.panel.add(this.btnSubmit);
    }

    public void createRadioBtn() {
        panel.add(new Label("Choose your type of game:"));

        this.radioBtn = new ButtonGroup();

        JRadioButton game1 = new JRadioButton("Solo");
        game1.setMnemonic(KeyEvent.VK_B);
        game1.setActionCommand(TypeGame.SOLO.getCode());
        game1.setSelected(true);
        this.radioBtn.add(game1);
        this.panel.add(game1);

        panel.add(new Label(""));
        JRadioButton game2 = new JRadioButton("Duo");
        game2.setMnemonic(KeyEvent.VK_B);
        game2.setActionCommand(TypeGame.DUO.getCode());
        game2.setSelected(true);
        this.radioBtn.add(game2);
        this.panel.add(game2);
    }

    public JPanel getPanel() {
        return panel;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
