package Main.Panel;

import Main.Map;
import Services.Data.Received.DataMap;
import Services.Service;

public class WaitThread extends Thread {
    private View view;
    private Service service;

    public WaitThread(View view, Service service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void run() {
        super.run();
        waitMap();
    }

    public void waitMap() {
        try {
            DataMap dataMap = null;
            while (dataMap == null) {
                Thread.sleep(100);
                dataMap = this.service.waitMap();
            }
            Map map = new Map(dataMap.getMap(), dataMap.getItems(), dataMap.getBombs());
            this.view.goBomberman(map);
            this.interrupt();
        } catch (Exception e) {
            System.err.println("[ERROR] Load map failed: " + e.toString());
        }
    }
}
