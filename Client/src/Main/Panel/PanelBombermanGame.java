package Main.Panel;

import Main.Info.AgentAction;
import Main.Map;
import Main.NetworkGame;
import Services.Service;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class PanelBombermanGame extends JPanel {
    private View view;
    private Service service;

    private NetworkGame game;
    private PanelBomberman panelBomberman;

    public PanelBombermanGame(View view, Service service, NetworkGame game, Map map) {
        this.game = game;
        this.view = view;
        this.service = service;
        this.panelBomberman = new PanelBomberman(map);

        this.addKeyBinding();
    }

    public void addKeyBinding() {
        // SPACE -> put bomb
        Action space = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.PUT_BOMB);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "space");
        this.panelBomberman.getActionMap().put("space", space);

        // Key Up
        Action up = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.MOVE_UP);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "up");
        this.panelBomberman.getActionMap().put("up", up);

        // Key Down
        Action down = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.MOVE_DOWN);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "down");
        this.panelBomberman.getActionMap().put("down", down);

        // Key Left
        Action left = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.MOVE_LEFT);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "left");
        this.panelBomberman.getActionMap().put("left", left);

        // Key Right
        Action right = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.MOVE_RIGHT);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "right");
        this.panelBomberman.getActionMap().put("right", right);

        // Key N
        Action next = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                game.keyboardAction(AgentAction.STOP);
            }
        };
        this.panelBomberman.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_N, 0), "next");
        this.panelBomberman.getActionMap().put("next", next);
    }

    public void update(Map map) {
        this.panelBomberman.setInfoGame(map.getStart_brokable_walls(),
                map.getStart_agents(),
                map.getItems(),
                map.getBombs());
        this.panelBomberman.repaint();
    }

    public JPanel getPanel() {
        return this.panelBomberman;
    }
}
