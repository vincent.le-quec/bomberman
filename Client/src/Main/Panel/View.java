package Main.Panel;

import Main.Map;
import Main.NetworkGame;
import Services.Service;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class View {
    private JFrame jFrame;
    private Service service;

    private NetworkGame game = null;

    public View(Service service) {
        this.service = service;
        this.jFrame = new JFrame();

        this.goLogin();

        this.jFrame.setLocation(0, 50);

        this.jFrame.setVisible(true);
        this.whenQuit();
    }

    public void whenQuit() {
        this.jFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if (game != null)
                    game.interrupt();
                service.exit();
                jFrame.setVisible(false);
                jFrame.dispose();
            }
        });
    }

    public void reset() {
        this.jFrame.dispose();
        this.jFrame = new JFrame();
        this.whenQuit();
    }

    public void goLogin() {
        if (this.game != null)
            this.game.interrupt();

        this.jFrame.setTitle("Bomberman - Login");
        this.jFrame.setSize(new Dimension(400, 200));
        this.jFrame.getContentPane().setBackground(Color.CYAN);
        this.switchPanel(new PanelLogin(this, this.service).getPanel());
    }

    public void goLoader() {
        WaitThread wait = new WaitThread(this, this.service);
        wait.start();
        this.jFrame.setTitle("Bomberman - Loading...");
        this.jFrame.setSize(new Dimension(450, 300));
        this.switchPanel(new PanelLoader(this, this.service).getPanel());
    }

    public void goBomberman(Map map) {
        this.game = new NetworkGame(map, this.service);
        PanelBombermanGame panelBomberman = new PanelBombermanGame(this, this.service, this.game, map);
        this.game.setPanelBomberman(panelBomberman);

        this.help();

        // Reset to activate keyboarding
        this.reset();
        this.jFrame.setTitle("Bomberman - Game");
        this.jFrame.setSize(new Dimension(map.getSizeX() * 40, map.getSizeY() * 40));

        this.switchPanel(this.game.getPanelBomberman());

        this.game.start();
    }

    public void switchPanel(JPanel panel) {
        this.jFrame.setContentPane(panel);
        this.jFrame.repaint();
        this.jFrame.revalidate();
        this.jFrame.setVisible(true);
    }

    private void help() {
        System.out.println("_-_-_-_-_Bomberman_-_-_-_-_");
        System.out.println(" - SPACE     - Put a bomb");
        System.out.println(" - KEY UP    - Move up");
        System.out.println(" - KEY DOWN  - Move down");
        System.out.println(" - KEY LEFT  - Move left");
        System.out.println(" - KEY RIGHT - Move right");
        System.out.println(" - N         - Skip turn");
    }
}

