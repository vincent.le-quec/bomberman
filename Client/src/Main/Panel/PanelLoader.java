package Main.Panel;

import Services.Service;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PanelLoader extends JPanel {
    private JPanel panel;
    private JLabel msg, picLabel;
    private BufferedImage image;

    private View view;
    private Service service;

    public PanelLoader(View view, Service service) {
        this.view = view;
        this.service = service;

        this.createView();
    }

    private void createView() {
        this.msg = new JLabel("Loading...", SwingConstants.CENTER);
        this.msg.setForeground(Color.lightGray);

        try {
            this.image = ImageIO.read(new File("./image/waiting.gif"));
            this.picLabel = new JLabel(new ImageIcon(this.image), SwingConstants.CENTER);
            // TODO: https://stackoverflow.com/questions/12566311/displaying-gif-animation-in-java
            this.panel = new JPanel();
            this.panel.setOpaque(true);
            this.panel.setBackground(Color.DARK_GRAY);
            this.panel.add(this.picLabel);
            this.panel.add(this.msg);
        } catch (IOException e) {
            System.out.println("[ERROR] Load waiting image:" + e.toString());
        }
    }

    public JPanel getPanel() {
        return panel;
    }
}
