package Services;

import Main.Panel.View;
import Services.Data.DataJSON;
import Services.Data.Received.*;
import Services.Data.Send.DataAction;
import Services.Data.Send.DataExit;
import Services.Data.Send.DataLogin;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * LOGIN -> rep (String) True | False
 * ACTION -> rep (String) null si illegal sinon la Map
 * EXIT -> rep (String)
 */

public class Service {
    private static final String ip = "127.0.0.1";
    private static final int port = 4242;

    private Socket so;
    private PrintWriter os;
    private BufferedReader br;

    private View view;

    public boolean yourTurn = false;
    public boolean gameOver = false;
    public boolean disableAction = true;

    public Service() {
        this.init();
    }

    public void init() {
        try {
            this.so = new Socket(ip, port);
            this.os = new PrintWriter(so.getOutputStream(), true);
            this.br = new BufferedReader(new InputStreamReader(so.getInputStream()));
        } catch (UnknownHostException e) {
            System.out.println("[ERROR] " + e.toString());
            System.exit(1);
        } catch (IOException e) {
            System.out.println("[ERROR] Aucun serveur n’est rattaché au port: " + e.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.toString());
            System.exit(1);
        }
    }

    public void login(String login, String password, String type) {
        String s = new DataLogin(login, password, type).stringify();
        System.out.println(s);
        this.os.println(s);
        this.os.flush();
    }

    public void action(String action) {
        this.os.println(new DataAction(action).stringify());
        this.os.flush();
    }

    public void exit() {
        this.os.println(new DataExit().stringify());
        try {
            this.br.close();
            this.so.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


    public String waitLogin() {
        String str = null;
        try {
            str = this.br.readLine();
        } catch (IOException e) {
            System.out.println("[ERROR] WaitLogin:" + e.toString());
        }
        return str;
    }

    public DataMap waitMap() {
        try {
            while (true) {
                String str = this.br.readLine();
//                if (str != null) {
                // TODO: fix this bullshit
                if (str != null && (str.equals("true") || str.equals("false"))) {
                    if (str.equals("true"))
                        this.view.goLoader();
                    return null;
                } else {
                    System.out.println("[Service::waitMap] Received: [" + str + "]");
                    ObjectMapper mapper = new ObjectMapper();
                    DataJSON data = mapper.readValue(str, DataJSON.class);

                    switch (data.getType()) {
                        case DataMap.key:
                            this.disableAction = false;
                            return mapper.readValue(data.getData(), DataMap.class);
                        case DataMyTurn.key:
                            this.yourTurn = true;
                            this.disableAction = false;
                            System.out.println("It's your turn");
                            break;
                        case DataVerifyMove.key:
                            System.out.println("Illegal move, try again");
                            this.disableAction = false;
                            this.yourTurn = true;
                            break;
                        case DataWaiting.key:
                            System.out.println("[Service::waitMap] I am waiting in the lobby");
                            break;
                        case DataInfoPlayer.key:
                            DataInfoPlayer info = mapper.readValue(data.getData(), DataInfoPlayer.class);
                            System.out.println("You're the " + info.getColor() + " Bomberman at position x=" + info.getX() + ", y=" + info.getY());
                            break;
                        case DataGameOver.key:
                            System.out.println("Game Over !");
                            this.gameOver = true;
                            DataGameOver go = mapper.readValue(data.getData(), DataGameOver.class);
                            System.out.println("Your score: " + go.getScore());
                            this.goToLogin();
                            break;
                        default:
                            return null;
                    }
                }

            }
        } catch (IOException e) {
            System.out.println("[ERROR] WaitMap:" + e.toString());
        }
        return null;
    }

    public void goToLogin() {
        this.exit();
        this.view.reset();
        this.init();
        this.view.goLogin();
    }

    public void setView(View view) {
        this.view = view;
    }
}
