package Services.Data;

import Services.Data.Send.DataAction;
import Services.Data.Send.DataLogin;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class Data {

    public abstract String stringify();

    public static Data objectify(String str) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            DataJSON json = mapper.readValue(str, DataJSON.class);
            Data info = null;
            switch (json.getType()) {
                case DataLogin.key:
                    info = mapper.readValue(json.getData(), DataLogin.class);
                    break;
                case DataAction.key:
                    info = mapper.readValue(json.getData(), DataAction.class);
                    break;
                case "EXIT":
                    // TODO:
                    break;
            }
            return info;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract String getKey();
}
