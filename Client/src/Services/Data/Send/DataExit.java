package Services.Data.Send;

import Services.Data.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DataExit extends Data {
    @JsonIgnore
    public static final String key = "EXIT";

    public DataExit() {
    }

    @Override
    public String stringify() {
        return "{\"type\":\"" + key + "\"}";
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}
