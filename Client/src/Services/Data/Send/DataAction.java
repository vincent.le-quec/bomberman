package Services.Data.Send;

import Services.Data.Data;
import Services.Data.DataJSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataAction extends Data {
    private String action;

    @JsonIgnore
    public static final String key = "ACTION";

    public DataAction() {
    }

    public DataAction(String action) {
        this.action = action;
    }

    @Override
    public String stringify() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            DataJSON json = new DataJSON(key, data);
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
