package Services.Data.Received;

import Services.Data.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DataWaiting extends Data {

    @JsonIgnore
    public static final String key = "WAITING";

    public DataWaiting() {}

    @Override
    public String stringify() {
        return "{\"type\":\"" + key + "\"}";
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}