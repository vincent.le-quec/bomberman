package Services.Data.Received;

import Services.Data.Data;
import Services.Data.DataJSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataVerifyMove extends Data {
    @JsonIgnore
    public static final String key = "VERIFY_MOVE";

    private boolean isValid;

    public DataVerifyMove() {}

    public DataVerifyMove(final boolean b) {
        this.isValid = b;
    }

    @Override
    public String stringify() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            DataJSON json = new DataJSON(key, data);
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}
