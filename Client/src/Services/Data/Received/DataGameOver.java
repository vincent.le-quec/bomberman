package Services.Data.Received;

import Services.Data.Data;
import Services.Data.DataJSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataGameOver extends Data {
    private int score;

    @JsonIgnore
    public static final String key = "GAME_OVER";

    public DataGameOver() {}

    public DataGameOver(int score) {
        this.score = score;
    }

    @Override
    public String stringify() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            DataJSON json = new DataJSON(key, data);
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
