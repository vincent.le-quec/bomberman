package Services.Data.Received;

import Services.Data.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DataMyTurn extends Data {

    @JsonIgnore
    public static final String key = "YOUR_TURN";

    public DataMyTurn() {}

    @Override
    public String stringify() {
        return "{\"type\":\"" + key + "\"}";
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }
}
