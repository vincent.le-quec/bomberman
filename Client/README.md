# Bomberman Client

## Scores
Action              | Score
:------------------:|:-----:
Kill a Bomberman    | 50
Kill a Bird         | 20
Kill a Raijon       | 10
Kill a Ennemi       | 5
Break a wall        | 1