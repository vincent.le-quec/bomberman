const path = require('path');

module.exports = {
    productionSourceMap: false,
    outputDir: path.resolve(__dirname, '../API/WebContent/'),
    "transpileDependencies": [
        "vuetify"
    ],
    publicPath: process.env.NODE_ENV === 'production'
        ? '/API/'
        : '/'
}