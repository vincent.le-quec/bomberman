import ApiUtils from "./utils";

const HOST = "http://localhost:8080/API";
const TOKEN = "FpbjSwXy4xOQbP7IGjrCGf75e6FAKGIW9CjhxYrpJwBcFHeLZVjJPIZQNumh2M1r";

export default {
    isEmailAvailable(login) {
        return fetch(`${HOST}/users?login=${encodeURI(login)}&token=${encodeURI(TOKEN)}`, {
            method: 'GET',
            cache: 'no-cache',
        }).then(ApiUtils.extract)
    },
    connectPlayer(login, password) {
        return fetch(`${HOST}/users?login=${encodeURI(login)}&password=${encodeURI(password)}&token=${encodeURI(TOKEN)}`, {
            method: 'GET',
            cache: 'no-cache',
        }).then(ApiUtils.extract)
    },
    getPlayer(id) {
        return fetch(`${HOST}/users?id=${encodeURI(id)}&token=${encodeURI(TOKEN)}`, {
            method: 'GET',
            cache: 'no-cache',
        }).then(ApiUtils.extract)
    },
    addPlayer(name, login, password) {
        return fetch(`${HOST}/users?token=${encodeURI(TOKEN)}&name=${encodeURI(name)}&login=${encodeURI(login)}&password=${encodeURI(password)}`, {
            method: 'PUT',
            headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',}),
            cache: 'no-cache',
         }).then(ApiUtils.extract)
    },
    updatePlayer(id, name, login, password) {
        return fetch(`${HOST}/users`, {
            method: 'POST',
            headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',}),
            body: "token=" + encodeURI(TOKEN) +
                "&id=" + encodeURI(id) +
                "&name=" + encodeURI(name) +
                "&login=" + encodeURI(login) +
                "&password=" + encodeURI(password),
            cache: 'no-cache',
         })
    },
    rmPlayer(id) {
        return fetch(`${HOST}/users?token=${encodeURI(TOKEN)}&id=${encodeURI(id)}`, {
            method: 'DELETE',
            headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',}),
            cache: 'no-cache',
         })
    },
    getGames() {
        return fetch(`${HOST}/games?token=${TOKEN}`, {
            method: 'GET',
            cache: 'no-cache',
        }).then(ApiUtils.extract)
    },
    getGamesWithID(id) {
        return fetch(`${HOST}/users?token=${TOKEN}&id=${id}`, {
            method: 'GET',
            cache: 'no-cache',
        }).then(ApiUtils.extract)
    }
}
