export default {
    extract: (res) => {
        return new Promise((resolve, reject) => {
            res.json().then(data => {
                resolve(data)
            }).catch(reject)
        })
    },
}