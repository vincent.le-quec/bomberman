import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.view.vue'
import Inscription from '../views/Inscription.view.vue'
import YourGames from '../views/YourGames.view'
import Connection from '../views/Connection.view.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/Inscription',
    name: 'Inscription',
    component: Inscription
  },
  {
    path: '/Connection',
    name: 'Connection',
    component: Connection
  },
  {
    path: '/YourGames',
    name: 'Your Games',
    component: YourGames
  },
  {
    path: '/Profil',
    name: 'Profil',
    component: Inscription
  }
]

const router = new VueRouter({
  routes
})

export default router
