import { SET_PLAYER } from "./mutations.type";

const mutations = {
    [SET_PLAYER](state, info) {
        state.player = info;
    }
};
export default mutations;