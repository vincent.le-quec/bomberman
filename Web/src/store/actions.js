import {SET_PLAYER} from "./mutations.type";
import {RESET_PLAYER} from "./actions.type";

const actions = {
    [RESET_PLAYER](context) {
        context.commit(SET_PLAYER, null);
    },
};
export default actions;