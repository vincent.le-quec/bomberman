package Server;

import Game.Info.AgentAction;
import Server.Data.Data;
import Server.Data.DataJSON;
import Server.Data.Received.DataAction;
import Server.Data.Received.DataExit;
import Server.thread.Server;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.Map;

public class Player {

    private Server server;
    private Socket socket;
    private final BufferedReader br;
    private final PrintWriter pw;
    private String name;
    private int id;
    private AgentAction action;
    private int score = 0;
    private boolean win = false;

    public Player(Socket socket, String name, int id) throws IOException {
        this.socket = socket;
        this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.pw = new PrintWriter(socket.getOutputStream(), true);
        this.name = name;
        this.id = id;
    }

    public Socket getSocket() {
        return socket;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Data read() {
        try {
//            System.out.println("[Player::read]waiting for message");
            String str = br.readLine();
//            System.out.println("[Player::read]Received: " + str);

            if (str != null) {

                ObjectMapper mapper = new ObjectMapper();
                DataJSON data = mapper.readValue(str, DataJSON.class);

                // At this point, the game is launched and we can only catch DataAction and DataExit requests
                switch (data.getType()) {
                    case DataAction.key:
//                        System.out.println("[Player::read] returns DataAction");
//                        System.out.println(mapper.readValue(data.getData(), DataAction.class));
                        return mapper.readValue(data.getData(), DataAction.class);

                    case DataExit.key:
                        System.out.println("[Player::read] returns DataExit");
                        server.removePlayer(this);
                        return mapper.readValue(data.getData(), DataExit.class);
                }

            }

        } catch (Exception ignored) {}

        System.out.println("[Player::read] returns DataExit");
        server.removePlayer(this);
        return new DataExit();
    }

    public AgentAction getAction() {
        return action;
    }

    public void setAction(AgentAction action) {
        this.action = action;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean hasWon() {
        return this.win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public void send(final Data data) {
        System.out.println("[Player:send] sending:" + data.getKey());
        this.send(data.stringify());
    }

    private void send(final String str) {
//        System.out.println("[Player:send] sending : " + str);
        pw.println(str);
        pw.flush();
    }

    public void close() throws IOException {
        br.close();
        pw.close();
        socket.close();
    }

    @Override
    public String toString() {
        return "Player{" +
                "socket=" + socket +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
