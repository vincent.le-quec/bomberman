package Server.Data.Received;

import Server.Data.Data;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DataExit extends Data {
    @JsonIgnore
    public static final String key = "EXIT";

    public DataExit() {
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }

    @Override
    public String stringify() {
        return "{\"type\":\"" + key + "\"}";
    }
}
