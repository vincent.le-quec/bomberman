package Server.Data.Send;

import Server.Data.Data;
import Server.Data.DataJSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataInfoPlayer extends Data {
    private String color;
    private int x;
    private int y;

    @JsonIgnore
    public static final String key = "INFO_PLAYER";

    public DataInfoPlayer() {}

    public DataInfoPlayer(String color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    @Override
    public String stringify() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            DataJSON json = new DataJSON(key, data);
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
