package Server.Data.Send;

import Game.Info.InfoAgent;
import Game.Info.InfoBomb;
import Game.Info.InfoItem;
import Server.Data.Data;
import Server.Data.DataJSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class DataMap extends Data {
    private String map;
    private List<InfoItem> items;
    private List<InfoBomb> bombs;
    private List<InfoAgent> players;

    @JsonIgnore
    public static final String key = "MAP";

    public DataMap() {
    }

    public DataMap(String map, List<InfoItem> items, List<InfoBomb> bombs, List<InfoAgent> players) {
        this.map = map;
        this.items = items;
        this.bombs = bombs;
        this.players = players;
    }

    @Override
    public String stringify() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            DataJSON json = new DataJSON(key, data);
            return mapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return key;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public List<InfoItem> getItems() {
        return items;
    }

    public void setItems(List<InfoItem> items) {
        this.items = items;
    }

    public List<InfoBomb> getBombs() {
        return bombs;
    }

    public void setBombs(List<InfoBomb> bombs) {
        this.bombs = bombs;
    }

    public List<InfoAgent> getPlayers() {
        return players;
    }

    public void setPlayers(List<InfoAgent> players) {
        this.players = players;
    }
}