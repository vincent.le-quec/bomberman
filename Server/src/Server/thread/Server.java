package Server.thread;

import Game.Agent.Agent;
import Game.Agent.AgentBomberman;
import Game.BombermanGame;
import Game.Info.AgentAction;
import Game.Info.InfoAgent;
import Game.Strategy.StrategyChoiceAgent;
import Server.Data.Data;
import Server.Data.Received.DataAction;
import Server.Data.Received.DataExit;
import Server.Data.Send.DataInfoPlayer;
import Server.Data.Send.DataMap;
import Server.Data.Send.DataVerifyMove;
import Server.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Server extends Thread {

    private List<Player> players = new ArrayList<>();
    private BombermanGame game;

    public Server(Player players) {
        this.players.add(players);
        players.setServer(this);
        this.game = new BombermanGame(1, this);
    }

    public Server(List<Player> players) {
        this.players = players;
        players.forEach(p -> p.setServer(this));
        this.game = new BombermanGame(this.players.size(), this);
    }

    public AgentAction checkInput(Player player) {
        // after 5 try, we make the player pass his turn
        int maxIteration = 5;
        int currentIteration = 0;

        while (currentIteration < maxIteration) {
            try {

                System.out.println("[Server::checkInput]waiting for message");
                Data data = player.read();

                if (data != null) {

                    switch (data.getKey()) {

                        case DataAction.key:
                            DataAction action = (DataAction) data;

                            // if illegal -> send illegal move
                            if (!game.isLegalMove(AgentAction.objectify(action.getAction()))) {

                                // if this is the last try, we do not send illegal because we are not waiting for another response
                                if(currentIteration != maxIteration - 1){
                                    player.send(new DataVerifyMove(false));
                                }

                                // else move agent and send the updated map
                            } else {
                                return AgentAction.objectify(action.getAction());
                            }
                            break;

                        case DataExit.key:
                            return AgentAction.STOP;
                    }
                }
            } catch (Exception e) {
            }
            currentIteration++;
        }
        System.out.println("[Server::checkInput] returns AgentAction.STOP");
        return AgentAction.STOP;
    }

    public void sendMap() {
        // TODO: fixes jackson bug
        List<InfoAgent> agents = this.game.getAgents()
                .stream()
                .map(InfoAgent::new)
                .collect(Collectors.toList());

        DataMap m = new DataMap(this.game.getMapToString(), this.game.getItems(), this.game.getBombs(), agents);
        System.out.println("[Server::sendMap]Send: " + m.stringify());
        for (Player player : this.players)
            player.send(m);
    }

    public void sendInfoPlayer(List<InfoAgent> agents) {
        for (InfoAgent infoAgent : agents) {
            Agent agent = (Agent) infoAgent;
            if (agent.getStrategyChoiceAgent() == StrategyChoiceAgent.USER_NETWORK && agent.getType() == 'B') {
                AgentBomberman ab = (AgentBomberman) agent;
                Player player = this.players
                        .stream()
                        .filter(p -> p.getId() == ab.getPlayerNumber())
                        .findAny()
                        .orElse(null);
                if (player != null) {
                    DataInfoPlayer data = new DataInfoPlayer(ab.getColor().toString(), ab.getX(), ab.getY());
                    System.out.println("[Server::sendInfo]Send: " + data.stringify());
                    player.send(data);
                }
            }
        }
    }

    public void removePlayer(final Player player) {
        game.killPlayer(player.getId());
        this.players.remove(player);
    }

    public void run() {
        try {
            Thread.sleep(500);
            this.game.launch();
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.toString());
        }
    }

    public List<Player> getPlayers() {
        return players;
    }
}
