package Server.thread;

import Server.Data.Send.DataWaiting;
import Server.Player;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class WaitingDuoGame extends Thread {
    private static final WaitingDuoGame waitingDuoGame = new WaitingDuoGame();
    private List<Player> players = new ArrayList<>();
    private BufferedReader br;
    private PrintWriter os;

    private WaitingDuoGame() {}

    public static WaitingDuoGame get() {
        System.out.println("[WaitingDuoGame::get] size=" + waitingDuoGame.players.size());
        return waitingDuoGame;
    }

    public static void addPlayer(Player player) {
        System.out.println("[WaitingDuoGame::addPlayer] players.size=" + waitingDuoGame.players.size());
        waitingDuoGame.players.add(player);
        if(waitingDuoGame.players.size() >= 2) {
            Player player1 = waitingDuoGame.players.get(0);
            Player player2 = waitingDuoGame.players.get(1);

            waitingDuoGame.players.remove(1);
            waitingDuoGame.players.remove(0);

            System.out.println("[WaitingDuoGame::addPlayer]" + player1);
            System.out.println("[WaitingDuoGame::addPlayer]" + player2);

            System.out.println("[WaitingDuoGame::addPlayer] start a new duoGame");
            List<Player> tmp = new ArrayList<>();
            tmp.add(player1);
            tmp.add(player2);
            new Server(tmp).start();
        } else {
            DataWaiting message = new DataWaiting();
            player.send(message);
        }
    }

}