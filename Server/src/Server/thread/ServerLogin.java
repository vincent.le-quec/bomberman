package Server.thread;

import Server.Data.DataJSON;
import Server.Data.Received.DataExit;
import Server.Data.Received.DataLogin;
import Server.Player;
import Server.APIConnection;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.*;

public class ServerLogin extends Thread {
    private Socket socket;
    private BufferedReader br;
    private PrintWriter os;

    public ServerLogin(Socket socket) {
        //System.out.println("[ServerLogin::ServerLogin]");
        this.socket = socket;
    }

    /**
     * return true if the connection has successfully been done
     * @return bool : can we quit connection (exit or good connection)
     * @throws IOException
     */
    public boolean quitConnection() throws IOException {
        System.out.println("[ServerLogin::checkInput]waiting for message");
        String str = this.br.readLine();

        if(str == null)
            return true;
//        System.out.println("[ServerLogin::checkInput]Received: " + str);

        ObjectMapper mapper = new ObjectMapper();
        DataJSON data = mapper.readValue(str, DataJSON.class);

        switch (data.getType()) {
            case DataLogin.key:
                DataLogin login = mapper.readValue(data.getData(), DataLogin.class);
                Player player = this.isConnectionAuthorised(login);


                // if connected -> type game 1 -> launch solo game
                //              -> type game 2 -> wait for other players
                // else         -> retry login

                // connection authorised
                if(player != null) {
                    this.sendMsg("true");

                    // redirect to solo game
                    if("1".equals(login.getGame())){
                        new Server(player).start();
                    // redirect to waiting room for duo game
                    } else {
                        WaitingDuoGame.addPlayer(player);
                    }

                    // quit login server
                    return true;

                // connection refused
                } else {
                    this.sendMsg("False");

                    // stay in login server for another try
                    return false;
                }
            case DataExit.key:
                // quit server login
                return true;
        }
        return false;
    }

    public void sendMsg(String str) {
        //System.out.println("[ServerLogin::sendMsg]Send: " + str);
        this.os.println(str);
    }

    public Player isConnectionAuthorised(DataLogin data) {
        int id = APIConnection.get().isConnectionAuthorised(data.getLogin(), data.getPassword());

        if(id != 0) {
            System.out.println(data.getLogin() + " is now connected.");
            try {
                return new Player(this.socket, data.getLogin(), id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void run() {
        try {
            //System.out.println("[ServerLogin::run] we are running");
            this.os = new PrintWriter(this.socket.getOutputStream(), true);
            this.br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            while (!quitConnection()) ;

        } catch (ConnectException e) {
            System.out.println("[ServerLogin::run][ERROR] Le serveur distant refuse la connexion (pas de serveur rattaché au port, ...):" + e.toString());
        } catch (NoRouteToHostException e) {
            System.out.println("[ServerLogin::run][ERROR] Problème pour joindre la machine distante (firewall, routage, ...):" + e.toString());
        } catch (SocketException e) {
            System.out.println("[ServerLogin::run][ERROR] Problème au niveau TCP:" + e.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
