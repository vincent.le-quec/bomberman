package Server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.Map;

public class APIConnection {
    private static boolean PRODUCTION_MODE = false;

    private static final APIConnection apiConnection = new APIConnection();
    private static final String GAME_SERVER_TOKEN = "2HslE8EiMXYDhMBJeJ1mSVdStpnkfLqqpkyze5uq4Nk5eGdpJAbPMcyfFWm5RVVr";
    private static final String ip = "127.0.0.1";
    private static final int port = 8080;
    private static final String PATH_BASE = "/API";
    private static final String PATH_GET_USER = PATH_BASE + "/users";
    private static final String PATH_ADD_GAME = PATH_BASE + "/games";
    private static final String PATH_ADD_SCORE = PATH_BASE + "/scores";
    private static final String FIELD_TOKEN = "token";
    private static final String FIELD_ID = "id";
    private static final String FIELD_LOGIN = "login";
    private static final String FIELD_PASSWORD = "password";
    private static final String FIELD_ROOM = "room";
    private static final String FIELD_ID_GAME = "id_game";
    private static final String FIELD_ID_USER = "id_user";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_WIN = "win";

    private APIConnection() {}

    public static APIConnection get() {
        return apiConnection;
    }

    /** Coonection to the API */
    public int isConnectionAuthorised(String login, String password) {

        if(!PRODUCTION_MODE)
            return 1;

        int id = 0;

        try {
            String args = URLEncoder.encode(FIELD_TOKEN, "UTF-8") + "=" + URLEncoder.encode(GAME_SERVER_TOKEN, "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_LOGIN, "UTF-8") + "=" + URLEncoder.encode(login, "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_PASSWORD, "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

            String payload = "GET " + PATH_GET_USER  + "?" + args + " HTTP/1.1\r\n"
                    + "Host: " + ip + ":" + port + "\n"
                    + "Connection: Close\n";

            System.out.println("[APIConnection::isConnectionAuthorised]Send authentication request to the API");
            String json = sendPayload(payload).split("Connection: close")[1];

            // parse response
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(json, Map.class);

            // set the id
            if(Boolean.TRUE.equals(map.get("result"))) {
                return (int)map.get("id");
            }
//            System.out.println("response=" + json);

        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println("[APIConnection::isConnectionAuthorised]connection authentication returns=" + id + " (0 if not found, else the player's id.");
        return id;
    }

    public int saveGame(String room) {

        if(!PRODUCTION_MODE)
            return 1;

        int id = 0;

        try {
            String args = URLEncoder.encode(FIELD_TOKEN, "UTF-8") + "=" + URLEncoder.encode(GAME_SERVER_TOKEN, "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_ROOM, "UTF-8") + "=" + URLEncoder.encode(room, "UTF-8");

            String payload = "PUT " + PATH_ADD_GAME + "?" + args + " HTTP/1.1\n"
                    + "Host: " + ip + ":" + port + "\n"
                    + "Content-Length: 0\n"
                    + "\n";

            System.out.println("[APIConnection::saveGame]Send the game to the database");
            String response = sendPayload(payload);
            String responseCode = response.substring(9, 12);
            String json = response.split("GMT")[1];

            // parse response
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(json, Map.class);

            // set the id
            if("201".equals(responseCode)) {
                id = (int)map.get(FIELD_ID);
            }
//            System.out.println("response=" + json);

        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println("[APIConnection::saveGame]game creation returns=" + id + " (0 if not found, else the game's id.");
        return id;
    }

    public int saveScore(int idGame, Player player) {

        if(!PRODUCTION_MODE)
            return 1;
        
        int id = 0;

        try {
            String args = URLEncoder.encode(FIELD_TOKEN, "UTF-8") + "=" + URLEncoder.encode(GAME_SERVER_TOKEN, "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_ID_GAME, "UTF-8") + "=" + URLEncoder.encode(Integer.toString(idGame), "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_ID_USER, "UTF-8") + "=" + URLEncoder.encode(Integer.toString(player.getId()), "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_SCORE, "UTF-8") + "=" + URLEncoder.encode(Integer.toString(player.getScore()), "UTF-8");
            args += "&" + URLEncoder.encode(FIELD_WIN, "UTF-8") + "=" + URLEncoder.encode(Boolean.toString(player.hasWon()), "UTF-8");

            String payload = "PUT " + PATH_ADD_SCORE + "?" + args + " HTTP/1.1\n"
                    + "Host: " + ip + ":" + port + "\n"
                    + "Content-Length: 0\n"
                    + "\n";

            System.out.println("[APIConnection::saveScore]Send the score to the database");
            String response = sendPayload(payload);
            String responseCode = response.substring(9, 12);
            String json = response.split("GMT")[1];

            // parse response
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(json, Map.class);

            // set the id
            if("201".equals(responseCode)) {
                id = (int)map.get(FIELD_ID);
            }
//            System.out.println("response=" + json);

        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println("[APIConnection::saveScore]score creation returns=" + id + " (0 if not found, else the game's id.");
        return id;
    }

    private String sendPayload(String payload) {

        StringBuilder res = new StringBuilder();

        try {

            Socket socket = new Socket(ip, port);
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
//            System.out.println(payload);
            pw.println(payload);
            pw.flush();

            BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String line;
            while ((line = rd.readLine()) != null) {
//                System.out.println("line=" + line);
                res.append(line);
            }

//            System.out.println("Response:");
//            System.out.println(res.toString());

            pw.close();
            rd.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        // "connection close" is the end of the header, after is the response,
        // so I split on it
        return res.toString();
    }
}
