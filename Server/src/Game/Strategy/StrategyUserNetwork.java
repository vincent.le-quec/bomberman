package Game.Strategy;

import Game.Agent.Agent;
import Game.BombermanGame;

public class StrategyUserNetwork extends Strategy {
    public StrategyUserNetwork(Agent agent) {
        super(agent);
    }

    @Override
    public void algo(BombermanGame game) {}
}
