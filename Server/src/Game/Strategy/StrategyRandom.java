package Game.Strategy;

import Game.Agent.Agent;
import Game.Agent.AgentBomberman;
import Game.BombermanGame;
import Game.Info.AgentAction;

import java.util.Random;

public class StrategyRandom extends Strategy {

    public StrategyRandom(Agent agent) {
        super(agent);
    }

    @Override
    public void algo(BombermanGame game) {
        if (!isKillingAgent(game)) {
            if (this.agent.getType() == 'V' && !this.birdFollow) {
                this.birdWakeUp(game);
            } else {
                AgentAction action = this.testRandomAction();
                if (game.isLegalMove(agent, action))
                    game.moveAgent(agent, action);
            }
        }
    }

    private AgentAction testRandomAction() {
        Random r = new Random();
        int nbActions = 4;
        if (this.agent.getType() == 'B') {
            AgentBomberman ab = (AgentBomberman) this.agent;
            if (ab.getNbBombs() > 0)
                nbActions = 5;
        }
        int rand = r.nextInt(nbActions);
        switch (rand) {
            case 0:
                return AgentAction.MOVE_UP;
            case 1:
                return AgentAction.MOVE_DOWN;
            case 2:
                return AgentAction.MOVE_LEFT;
            case 3:
                return AgentAction.MOVE_RIGHT;
            case 4:
                return AgentAction.PUT_BOMB;
        }
        return AgentAction.STOP;
    }
}
