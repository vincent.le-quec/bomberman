package Game.Strategy;

import Game.Agent.Agent;
import Game.BombermanGame;

public class StrategyUser extends Strategy {
    public StrategyUser(Agent agent) {
        super(agent);
    }

    @Override
    public void algo(BombermanGame game) {}
}
