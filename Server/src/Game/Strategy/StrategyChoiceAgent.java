package Game.Strategy;

public enum StrategyChoiceAgent {
    RANDOM,
    USER,
    AI_TARGET,   // A* like to target the Bomberman
    USER_NETWORK
}
