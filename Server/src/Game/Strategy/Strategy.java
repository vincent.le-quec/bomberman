package Game.Strategy;

import Game.Agent.Agent;
import Game.Agent.AgentBird;
import Game.BombermanGame;
import Game.Info.AgentAction;
import Game.Info.InfoAgent;

public abstract class Strategy {
    protected Agent agent;
    protected boolean birdFollow = false;

    public Strategy(Agent agent) {
        this.agent = agent;
    }

    public abstract void algo(BombermanGame game);

    protected boolean isKillingAgent(BombermanGame game) {
        if (this.agent.getType() != 'B') {
            int x = this.agent.getX();
            int y = this.agent.getY();

            if (this.agent.isAgentOnPath(game.getMap(), x + 1, y, 'B')) {
                this.killAgentAndMove(game, x + 1, y, AgentAction.MOVE_RIGHT);
                return true;
            } else if (this.agent.isAgentOnPath(game.getMap(), x - 1, y, 'B')) {
                this.killAgentAndMove(game, x - 1, y, AgentAction.MOVE_LEFT);
                return true;
            } else if (this.agent.isAgentOnPath(game.getMap(), x, y - 1, 'B')) {
                this.killAgentAndMove(game, x, y - 1, AgentAction.MOVE_UP);
                return true;
            } else if (this.agent.isAgentOnPath(game.getMap(), x, y + 1, 'B')) {
                this.killAgentAndMove(game, x, y + 1, AgentAction.MOVE_DOWN);
                return true;
            }
        }
        return false;
    }

    protected void killAgentAndMove(BombermanGame game, int x, int y, AgentAction action) {
        for (InfoAgent ag : game.getAgents())
            if (!ag.isInvincible() && ag.getX() == x && ag.getY() == y) {
                ag.setDead(true);
                game.moveAgent(this.agent, action);
            }
    }

    protected void birdWakeUp(BombermanGame game) {
        AgentBird bird = (AgentBird) this.agent;
        int range = bird.getRange();
        int x = (bird.getX() - range > 0) ? bird.getX() - range : 0;
        int y = (bird.getY() - range > 0) ? bird.getY() - range : 0;
        int xMax = (bird.getX() + range < game.getMap().getSizeX()) ? bird.getX() + range : game.getMap().getSizeX();
        int yMax = (bird.getY() + range < game.getMap().getSizeY()) ? bird.getY() + range : game.getMap().getSizeY();

        for (int i = y; i <= yMax; i++)
            for (int j = x; j <= xMax; j++)
                if (bird.isAgentOnPath(game.getMap(), j, i, 'B')) {
                    birdFollow = true;
                    return;
                }
    }

}
