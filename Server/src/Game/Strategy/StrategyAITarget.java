package Game.Strategy;

import Game.Agent.Agent;
import Game.BombermanGame;
import Game.Info.AgentAction;
import Game.Info.InfoAgent;

import java.util.HashMap;
import java.util.Map;

public class StrategyAITarget extends Strategy {

    public StrategyAITarget(Agent agent) {
        super(agent);
    }

    @Override
    public void algo(BombermanGame game) {
        if (!this.isKillingAgent(game)) {
            if (this.agent.getType() == 'V' && !this.birdFollow) {
                this.birdWakeUp(game);
            } else {
                // Find target
                Agent target = this.findTarget(game);
                if (target != null) {
                    // Find good AgentAction
                    AgentAction action = this.findAction(game, target);
                    // Apply action
                    if (game.isLegalMove(this.agent, action))
                        game.moveAgent(agent, action);
                }
            }
        }
    }

    private AgentAction findAction(BombermanGame game, Agent target) {
        Map<AgentAction, Double> dist = new HashMap<>();

        if (game.isLegalMove(agent, AgentAction.MOVE_UP))
            dist.put(AgentAction.MOVE_UP, testDistAction(this.agent.getX(), this.agent.getY() - 1, target));
        if (game.isLegalMove(agent, AgentAction.MOVE_DOWN))
            dist.put(AgentAction.MOVE_DOWN, testDistAction(this.agent.getX(), this.agent.getY() + 1, target));
        if (game.isLegalMove(agent, AgentAction.MOVE_LEFT))
            dist.put(AgentAction.MOVE_LEFT, testDistAction(this.agent.getX() - 1, this.agent.getY(), target));
        if (game.isLegalMove(agent, AgentAction.MOVE_RIGHT))
            dist.put(AgentAction.MOVE_RIGHT, testDistAction(this.agent.getX() + 1, this.agent.getY(), target));

        if (dist.isEmpty())
            return AgentAction.STOP;
        else
            return dist.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue())
                    .findFirst()
                    .get()
                    .getKey();
    }

    private double testDistAction(int x, int y, Agent target) {
        int xBck = this.agent.getX();
        int yBck = this.agent.getY();

        // Set new position
        this.agent.setX(x);
        this.agent.setY(y);

        double res = distAgentToBomberman(target);
        // Reset
        this.agent.setX(xBck);
        this.agent.setY(yBck);
        return res;
    }

    private Agent findTarget(BombermanGame game) {
        double min = -1, tmp;
        InfoAgent target = null;

        for (InfoAgent a : game.getAgents()) {
            if (a.getType() == 'B') {
                tmp = this.distAgentToBomberman((Agent) a);
                if (min == -1 || tmp < min) {
                    min = tmp;
                    target = a;
                }
            }
        }
        return (Agent) target;
    }

    private double distAgentToBomberman(Agent bomberman) {
        return Math.abs(Math.sqrt(Math.pow(bomberman.getX() - this.agent.getX(), 2) + Math.pow(bomberman.getY() - this.agent.getY(), 2)));
    }

}
