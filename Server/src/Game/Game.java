package Game;

public abstract class Game implements Runnable {
    private int turn = 0;
    private int maxTurn;
    private boolean isRunning;

    public Thread thread;

    public Game(int maxTurn) {
        this.maxTurn = maxTurn;
        this.init();
    }

    public void init() {
        this.turn = 0;
        this.initializeGame();
//        this.notifyObserver();
    }

    abstract void initializeGame();

    abstract void takeTurn();

    abstract void gameOver(String s);

    abstract boolean gameContinue();

    public void step() {
        this.turn++;
        if (this.gameContinue()) {
            this.takeTurn();
        } else {
            this.isRunning = false;
            this.gameOver("Game Over!");
        }
    }

    public void run() {
        this.notifyObserver();
        while (this.isRunning)
            this.step();
    }

    public void stop() {
        this.isRunning = false;
    }

    public void launch() {
        this.isRunning = true;
        this.thread = new Thread(this);
        this.thread.start();
    }

    public abstract void notifyObserver();

    /* Getters & Setters */
    public int getTurn() {
        return turn;
    }

    public int getMaxTurn() {
        return maxTurn;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
