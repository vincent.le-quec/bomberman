package Game;

import Game.Agent.*;
import Game.Info.*;
import Game.Strategy.StrategyChoiceAgent;
import Server.APIConnection;
import Server.Data.Send.DataGameOver;
import Server.Data.Send.DataMyTurn;
import Server.Player;
import Server.thread.Server;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class BombermanGame extends Game {
    private static final String MAP_FOLDER = "layouts/";
    private static final double PROB_ITEM = 0.35;

    private Map map;
    private int id;

    private List<InfoAgent> agents;
    private List<InfoItem> items;
    private List<InfoBomb> bombs;
    private StrategyChoiceAgent strategyChoice;
    private int nbPlayers;
    private int nbTurnSick;
    private int nbTurnInvincible;

    private AgentBomberman agentPlayer = null;
    private boolean isEnnemies = false;

    private Server server;

    public BombermanGame(int nbPlayers, Server server) {
        super(Integer.MAX_VALUE - 1);
        try {
            String room;
            if (nbPlayers == 1)
                room = "niveau2.lay";
            else
                room = "jeu1.lay";

            this.map = new Map(MAP_FOLDER + room);
            this.agents = new ArrayList<>();
            this.server = server;
            this.items = new ArrayList<>();
            this.bombs = new ArrayList<>();
            this.nbPlayers = nbPlayers;
            this.nbTurnSick = 0;
            this.nbTurnInvincible = 0;
            this.strategyChoice = StrategyChoiceAgent.AI_TARGET;
            this.createAgents(); // ici sinon on n'a pas la map...
            this.isEnnemies = (this.nbEnemies() > 0);
            this.server.sendInfoPlayer(this.agents);

            // send this game to the api
            this.id = APIConnection.get().saveGame(room);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isLegalMove(AgentAction action) {
        return this.isLegalMove(this.agentPlayer, action);
    }

    /**
     * Verifie si l'action est possible
     *
     * @param a
     * @param action
     * @return boolean
     */
    public boolean isLegalMove(Agent a, AgentAction action) {
        boolean isLegal = false;

        switch (action) {
            case MOVE_UP:
                isLegal = a.canMoveUp(this.map);
                break;
            case MOVE_DOWN:
                isLegal = a.canMoveDown(this.map);
                break;
            case MOVE_LEFT:
                isLegal = a.canMoveLeft(this.map);
                break;
            case MOVE_RIGHT:
                isLegal = a.canMoveRight(this.map);
                break;
            case STOP:
                isLegal = a.canStop(this.map);
                break;
            case PUT_BOMB:
                isLegal = a.canPutBomb(this.map);
                break;
            default:
                System.out.println("[WARN] This action doesn't exist");
                break;
        }
        return isLegal;
    }

    /**
     * Effectue un mouvement à un agent
     *
     * @param a
     * @param action
     */
    public void moveAgent(Agent a, AgentAction action) {
        switch (action) {
            case MOVE_UP:
                this.map = a.moveUp(this.map);
                if (a.getType() == 'B')
                    this.isItemOnPath(this.map, (AgentBomberman) a, a.getX(), a.getY());
                break;
            case MOVE_DOWN:
                this.map = a.moveDown(this.map);
                if (a.getType() == 'B')
                    this.isItemOnPath(this.map, (AgentBomberman) a, a.getX(), a.getY());
                break;
            case MOVE_LEFT:
                this.map = a.moveLeft(this.map);
                if (a.getType() == 'B')
                    this.isItemOnPath(this.map, (AgentBomberman) a, a.getX(), a.getY());
                break;
            case MOVE_RIGHT:
                this.map = a.moveRight(this.map);
                if (a.getType() == 'B')
                    this.isItemOnPath(this.map, (AgentBomberman) a, a.getX(), a.getY());
                break;
            case STOP:
                this.map = a.stop(this.map);
                break;
            case PUT_BOMB:
                AgentBomberman ab = (AgentBomberman) a;
                this.map = ab.putBomb(this.map);
                this.createBombe(ab.getX(), ab.getY(), ab.getRange(), ab.getPlayerNumber());
                ab.rmBomb();
                break;
            default:
                System.out.println("[WARN] This action doesn't exist");
                break;
        }
    }

    protected void isItemOnPath(Map map, AgentBomberman a, int x, int y) {
        Iterator<InfoItem> iterator = this.items.iterator();
        InfoItem i = null;

        while (iterator.hasNext()) {
            i = iterator.next();
            if (i.getX() == x && i.getY() == y) {
                switch (i.getType()) {
                    case FIRE_UP:
                        System.out.println("Item FIRE_UP ramassé");
                        System.out.println("Range : " + a.getRange() + " -> " + (a.getRange() + 1));
                        a.setRangeUp();
                        iterator.remove();
                        break;
                    case FIRE_DOWN:
                        System.out.println("Item FIRE_DOWN ramassé");
                        System.out.println("Range : " + a.getRange() + " -> " + (a.getRange() - 1));
                        a.setRangeDown();
                        iterator.remove();
                        break;
                    case BOMB_UP:
                        System.out.println("Item BOMB_UP ramassé");
                        System.out.println("Nombre de bombes : " + a.getNbBombs() + " -> " + (a.getNbBombs() + 1));
                        a.setNbBombsUp();
                        iterator.remove();
                        break;
                    case BOMB_DOWN:
                        System.out.println("Item BOMB_DOWN ramassé");
                        System.out.println("Nombre de bombes : " + a.getNbBombs() + " -> " + (a.getNbBombs() - 1));
                        a.setNbBombsDown();
                        iterator.remove();
                        break;
                    case FIRE_SUIT:
                        System.out.println("Item FIRE_SUIT ramassé");
                        a.setInvincible(true);
                        this.nbTurnInvincible = 10;
                        iterator.remove();
                        break;
                    case SKULL:
                        System.out.println("Item SKULL ramassé");
                        a.setSick(true);
                        this.nbTurnSick = 10;
                        iterator.remove();
                        break;
                }
            }
        }
    }

    public void notifyObserver() {
        this.server.sendMap();
    }

    /**
     * Création des agents à partir de leur position sur la map et leur type
     */
    private void createAgents() {
        if (this.nbPlayers > this.nbBombermanMap())
            throw new IllegalArgumentException("Too many players");

        int nb = 0;
        Iterator<InfoAgent> iterator = this.map.getStart_agents().iterator();
        InfoAgent a = null;

        while (iterator.hasNext()) {
            a = iterator.next();
            // B: Bomberman; E: ennemi; R: rajion; V: bird
            switch (a.getType()) {
                case 'B':
                    this.agents.add(new AgentBomberman(
                            a.getX(),
                            a.getY(),
                            a.getAgentAction(),
                            a.getColor(),
                            a.isInvincible(),
                            a.isSick(),
                            ((nb < this.nbPlayers) ? StrategyChoiceAgent.USER_NETWORK : StrategyChoiceAgent.RANDOM),
                            ((nb < this.nbPlayers) ? this.server.getPlayers().get(nb).getId() : nb)
                    ));
                    nb++;
                    break;
                case 'E':
                    this.agents.add(new AgentEnnemi(
                            a.getX(),
                            a.getY(),
                            a.getAgentAction(),
                            a.getColor(),
                            a.isInvincible(),
                            a.isSick(),
                            this.strategyChoice
                    ));
                    break;
                case 'R':
                    this.agents.add(new AgentRajion(
                            a.getX(),
                            a.getY(),
                            a.getAgentAction(),
                            a.getColor(),
                            a.isInvincible(),
                            a.isSick(),
                            this.strategyChoice
                    ));
                    break;
                case 'V':
                    this.agents.add(new AgentBird(
                            a.getX(),
                            a.getY(),
                            a.getAgentAction(),
                            a.getColor(),
                            a.isInvincible(),
                            a.isSick(),
                            this.strategyChoice
                    ));
                    break;
                default:
                    System.out.println("[WARN] This agent doesn't exist");
                    iterator.remove();
                    break;
            }
        }
    }

    private int nbBombermanMap() {
        int nb = 0;

        for (InfoAgent a : this.map.getStart_agents())
            if (a.getType() == 'B')
                nb++;
        return nb;
    }

    private int nbBombermanPlayer() {
        int nb = 0;

        for (InfoAgent a : this.agents)
            if (a.getType() == 'B') {
                AgentBomberman ab = (AgentBomberman) a;
                if (ab.getStrategyChoiceAgent() == StrategyChoiceAgent.USER_NETWORK)
                    nb++;
            }
        return nb;
    }

    private int nbEnemies() {
        int nb = 0;

        for (InfoAgent a : this.agents)
            if (a.getType() != 'B')
                nb++;
        return nb;
    }

    /**
     * Initialise le jeu et doit créer les agents à partir de leur position
     */
    @Override
    void initializeGame() {
//        System.out.println("[DEBUG] initializeGame");
    }

    /**
     * Effectue une action pour chaque agent puis met à jour le plateau
     * Supprime l'agent s'il est mort
     */
    @Override
    void takeTurn() {
        Iterator<InfoAgent> iterator = this.agents.iterator();
        Agent agent = null;

        this.bombEvolution();
//        this.notifyObserver();
        while (iterator.hasNext()) {
            agent = (Agent) iterator.next();

            // remove agent if dead
            if (agent.isDead()) {
                // Set score when Bomberman die
                if (agent.getStrategyChoiceAgent() == StrategyChoiceAgent.USER_NETWORK && agent.getType() == 'B') {
                    AgentBomberman ab = (AgentBomberman) agent;
                    this.server.getPlayers()
                            .stream()
                            .filter(p -> p.getId() == ab.getPlayerNumber())
                            .findAny().ifPresent(player -> player.setScore(ab.getScore()));
                }
                this.map.killAgent(agent.getX(), agent.getY());
                iterator.remove();
                // otherwise execute its move
            } else {
                if (agent.getStrategyChoiceAgent() == StrategyChoiceAgent.USER_NETWORK && agent.getType() == 'B') {
                    this.agentPlayer = (AgentBomberman) agent;
                    this.agentPlayer.algo(this);
                    System.out.println("-- Player " + this.agentPlayer.getPlayerNumber() + " --");

                    // move an agent
                    Player player = this.server.getPlayers()
                            .stream()
                            .filter(p -> p.getId() == this.agentPlayer.getPlayerNumber())
                            .findAny()
                            .orElseThrow(NullPointerException::new);
                    System.out.println("Your Turn");
                    player.send(new DataMyTurn());

                    AgentAction aa = this.server.checkInput(player);

                    System.out.println("[BombermanGame::takeTurn] Action: " + aa.toString());

                    this.moveAgent(this.agentPlayer, aa);
                    player.setAction(null);

                    // handle sickness
                    if (this.agentPlayer.isSick()) {
                        System.out.println(" Bomberman malade...");
                        this.nbTurnSick--;
                        if (this.nbTurnSick == 0) this.agentPlayer.setSick(false);
                    }

                    // handle invisible
                    if (this.agentPlayer.isInvincible()) {
                        System.out.println(" Bomberman invincible !");
                        this.nbTurnInvincible--;
                        if (this.nbTurnInvincible == 0) this.agentPlayer.setInvincible(false);
                    }
                    // Send map only when Bomberman play
                    System.out.println("Send map only when Bomberman play");
                    this.notifyObserver();
                } else
                    agent.algo(this);
            }
        }
    }

    private AgentBomberman findBomberman(int id) {
        for (InfoAgent agent : agents)
            if (agent.getType() == 'B') {
                AgentBomberman ab = (AgentBomberman) agent;
                if (ab.getPlayerNumber() == id)
                    return ab;
            }
        return null;
    }

    /*********
     * BOMBS *
     *********/
    public void createBombe(int x, int y, int range, int id) {
        this.bombs.add(new InfoBomb(x, y, range, StateBomb.Step1, id));
    }

    private void bombEvolution() {
        Iterator<InfoBomb> it = this.bombs.iterator();
        InfoBomb bomb = null;

        while (it.hasNext()) {
            bomb = it.next();
            switch (bomb.getStateBomb()) {
                case Step1:
                    bomb.setStateBomb(StateBomb.Step2);
                    break;
                case Step2:
                    bomb.setStateBomb(StateBomb.Step3);
                    break;
                case Step3:
                    bomb.setStateBomb(StateBomb.Boom);
                    break;
                case Boom:
                    breakWallsAndKill(bomb);
                    it.remove();
                    break;
            }
        }
    }

    private void breakWallsAndKill(InfoBomb bomb) {
        int x = bomb.getX();
        int y = bomb.getY();
        int range = bomb.getRange();
        AgentBomberman ab = this.findBomberman(bomb.getUserId());

        if (ab != null) {
            // Up
            for (int i = 0; i <= range && y > 0; i++, y--) {
                if (map.getStart_brokable_walls()[x][y]) {
                    this.spawnItem(x, y);
                    map.breakWall(x, y);
                    ab.addScore(1);
                } else
                    ab.addScore(this.killEnnemyOnPath(x, y, bomb.getUserId()));
            }
            y = bomb.getY();

            // Down
            for (int i = 0; i <= range && y < map.getSizeY() - 1; i++, y++) {
                if (map.getStart_brokable_walls()[x][y]) {
                    this.spawnItem(x, y);
                    map.breakWall(x, y);
                    ab.addScore(1);
                } else
                    ab.addScore(this.killEnnemyOnPath(x, y, bomb.getUserId()));
            }
            y = bomb.getY();

            // Left
            for (int i = 0; i <= range && x > 0; i++, x--) {
                if (map.getStart_brokable_walls()[x][y]) {
                    this.spawnItem(x, y);
                    map.breakWall(x, y);
                    ab.addScore(1);
                } else
                    ab.addScore(this.killEnnemyOnPath(x, y, bomb.getUserId()));
            }
            x = bomb.getX();

            // Right
            for (int i = 0; i <= range && x < map.getSizeX() - 1; i++, x++) {
                if (map.getStart_brokable_walls()[x][y]) {
                    this.spawnItem(x, y);
                    map.breakWall(x, y);
                    ab.addScore(1);
                } else
                    ab.addScore(this.killEnnemyOnPath(x, y, bomb.getUserId()));
            }
        }
        System.out.println("SCORE: " + ab.getScore());
    }

    public int killEnnemyOnPath(int x, int y, int userId) {
        int pts = 0;
        for (InfoAgent agent : agents)
            if (agent.getType() == 'B') {
                AgentBomberman ab = (AgentBomberman) agent;
                if (ab.getPlayerNumber() != userId && !agent.isInvincible() && agent.getX() == x && agent.getY() == y) {
                    agent.setDead(true);
                    pts += 50;
                }
            } else if (!agent.isInvincible() && agent.getX() == x && agent.getY() == y) {
                agent.setDead(true);
                // B: Bomberman; E: ennemi; R: rajion; V: bir
                switch (agent.getType()) {
                    case 'E':
                        pts += 5;
                        break;
                    case 'R':
                        pts += 10;
                        break;
                    case 'V':
                        pts += 20;
                        break;
                }
            }
        return pts;
    }

    /**
     * Codes objects :
     * 1 -> fire_up
     * 2 -> fire_down
     * 3 -> bomb_up
     * 4 -> bomb_down
     * 5 -> fire_suit
     * 6 -> skull
     */
    private void spawnItem(int x, int y) {
        Random rand = new Random();
        double nb = rand.nextDouble();

        // Arrondi à 2 chiffres après la virgule
        BigDecimal bd = new BigDecimal(Double.toString(nb));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        double chance = bd.doubleValue();

        if (chance < PROB_ITEM) {
            rand = new Random();
            int item = rand.nextInt(6) + 1;
            switch (item) {
                case 1:
                    this.items.add(new InfoItem(x, y, ItemType.FIRE_UP));
                    System.out.println("Apparition de l'objet FIRE_UP");
                    break;
                case 2:
                    this.items.add(new InfoItem(x, y, ItemType.FIRE_DOWN));
                    System.out.println("Apparition de l'objet FIRE_DOWN");
                    break;
                case 3:
                    this.items.add(new InfoItem(x, y, ItemType.BOMB_UP));
                    System.out.println("Apparition de l'objet BOMB_UP");
                    break;
                case 4:
                    this.items.add(new InfoItem(x, y, ItemType.BOMB_DOWN));
                    System.out.println("Apparition de l'objet BOMB_DOWN");
                    break;
                case 5:
                    this.items.add(new InfoItem(x, y, ItemType.FIRE_SUIT));
                    System.out.println("Apparition de l'objet FIRE_SUIT");
                    break;
                case 6:
                    this.items.add(new InfoItem(x, y, ItemType.SKULL));
                    System.out.println("Apparition de l'objet SKULL");
                    break;
            }
        }

    }

    @Override
    void gameOver(String s) {
        this.notifyObserver();
        for (InfoAgent a : this.agents) {
            Agent agent = (Agent) a;
            if (agent.getStrategyChoiceAgent() == StrategyChoiceAgent.USER_NETWORK && agent.getType() == 'B') {
                AgentBomberman ab = (AgentBomberman) agent;
                this.server.getPlayers()
                        .stream()
                        .filter(p -> p.getId() == ab.getPlayerNumber())
                        .findAny().ifPresent(player -> {
                            player.setScore(ab.getScore());
                            player.setWin(true);
                        }
                );
            }
        }

        for (Player player : this.server.getPlayers()) {
            player.send(new DataGameOver(player.getScore()));
            // save the score
            APIConnection.get().saveScore(this.id, player);
        }
        System.out.println(s);
    }

    @Override
    public boolean gameContinue() {
        if (isEnnemies)
            return (this.nbBombermanPlayer() > 0 && this.nbEnemies() > 0 && super.getTurn() < super.getMaxTurn());
        return (this.nbBombermanPlayer() > 0 && super.getTurn() < super.getMaxTurn());
    }

    public Map getMap() {
        return this.map;
    }

    public String getMapToString() {
        return this.map.toString();
    }

    public List<InfoItem> getItems() {
        return this.items;
    }

    public List<InfoBomb> getBombs() {
        return this.bombs;
    }

    public List<InfoAgent> getAgents() {
        return this.agents;
    }

    public void killPlayer(int id) {
        this.agents
                .stream()
                .filter(a -> a.getType() == 'B' && ((AgentBomberman) a).getPlayerNumber() == id)
                .findFirst()
                .orElseThrow(NullPointerException::new)
                .setDead(true);
    }

}
