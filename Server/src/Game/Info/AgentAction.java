package Game.Info;

public enum AgentAction {
    MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, STOP, PUT_BOMB;

    public static AgentAction objectify(String action) {
        switch (action) {
            case "MOVE_UP":     return MOVE_UP;
            case "MOVE_DOWN":   return MOVE_DOWN;
            case "MOVE_LEFT":   return MOVE_LEFT;
            case "MOVE_RIGHT":  return MOVE_RIGHT;
            case "STOP":        return STOP;
            case "PUT_BOMB":    return PUT_BOMB;
            default:            return null;
        }
    }
}
