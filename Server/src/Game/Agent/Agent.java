package Game.Agent;

import Game.BombermanGame;
import Game.Info.AgentAction;
import Game.Info.ColorAgent;
import Game.Info.InfoAgent;
import Game.Map;
import Game.Strategy.*;

import java.util.List;

public abstract class Agent extends InfoAgent {
    protected Strategy strategy;
    protected StrategyChoiceAgent strategyChoiceAgent;


    public Agent(int x, int y, AgentAction agentAction, char type, ColorAgent color, boolean isInvincible, boolean isSick, StrategyChoiceAgent choice) {
        super(x, y, agentAction, type, color, isInvincible, isSick);
        this.strategyChoiceAgent = choice;
        this.loadStrategy(choice);
    }

    protected void setPosition(int x, int y) {
        super.setX(x);
        super.setY(y);
    }

    protected int findIdAgent(Map map) {
        List<InfoAgent> agents = map.getStart_agents();

        for (int i = 0; i < agents.size(); i++) {
            if (agents.get(i).getX() == this.getX() &&
                    agents.get(i).getY() == this.getY() &&
                    agents.get(i).getType() == this.getType())
                return i;
        }
        return -1;
    }

    protected boolean isAgentOnPath(Map map, int x, int y) {
        List<InfoAgent> agents = map.getStart_agents();

        for (InfoAgent agent : agents)
            if (agent.getX() == x && agent.getY() == y)
                return true;
        return false;
    }

    public boolean isAgentOnPath(Map map, int x, int y, char type) {
        List<InfoAgent> agents = map.getStart_agents();

        for (InfoAgent agent : agents) {
            if (agent.getType() == type && agent.getX() == x && agent.getY() == y)
                return true;
        }
        return false;
    }

    protected boolean checkLimits(Map map, int x, int y) {
        return !this.isAgentOnPath(map, x, y) &&
                x >= 0 && x < map.getSizeX() &&
                y >= 0 && y < map.getSizeY() &&
                !map.get_walls()[x][y] &&
                !map.getStart_brokable_walls()[x][y];
    }

    public boolean canMoveUp(Map map) {
        // si mur en haut -> false
        // sinon -> true
        int x = this.getX();
        int y = this.getY() - 1;
        return this.checkLimits(map, x, y);
    }

    public boolean canMoveDown(Map map) {
        // si mur en bas -> false
        // sinon -> true
        int x = this.getX();
        int y = this.getY() + 1;

        return this.checkLimits(map, x, y);
    }

    public boolean canMoveLeft(Map map) {
        // si mur à droite -> false
        // sinon -> true
        int x = this.getX() - 1;
        int y = this.getY();

        return this.checkLimits(map, x, y);
    }

    public boolean canMoveRight(Map map) {
        // si mur à gauche -> false
        // sinon -> true
        int x = this.getX() + 1;
        int y = this.getY();

        return this.checkLimits(map, x, y);
    }

    public boolean canStop(Map map) {
        return true;
    }

    public boolean canPutBomb(Map map) {
        return false;
    }


    // INFO: Map: 0,0 top left -> n,n (n > 0) bottom right
    public Map moveUp(Map map) {
        int id = this.findIdAgent(map);

        if (id != -1) {
            map.setAgentPosition(id, this.getX(), this.getY() - 1);
            this.setPosition(this.getX(), this.getY() - 1);
            super.setAgentAction(AgentAction.MOVE_UP);
        }
        return map;
    }

    public Map moveDown(Map map) {
        int id = this.findIdAgent(map);

        if (id != -1) {
            map.setAgentPosition(id, this.getX(), this.getY() + 1);
            this.setPosition(this.getX(), this.getY() + 1);
            super.setAgentAction(AgentAction.MOVE_DOWN);
        }
        return map;
    }

    public Map moveLeft(Map map) {
        int id = this.findIdAgent(map);

        if (id != -1) {
            map.setAgentPosition(id, this.getX() - 1, this.getY());
            this.setPosition(this.getX() - 1, this.getY());
            super.setAgentAction(AgentAction.MOVE_LEFT);
        }
        return map;
    }

    public Map moveRight(Map map) {
        int id = this.findIdAgent(map);

        if (id != -1) {
            map.setAgentPosition(id, this.getX() + 1, this.getY());
            this.setPosition(this.getX() + 1, this.getY());
            super.setAgentAction(AgentAction.MOVE_RIGHT);
        }
        return map;
    }

    public Map stop(Map map) {
        super.setAgentAction(AgentAction.STOP);
        return map;
    }

    public Map putBomb(Map map) {
        // TODO: return error
        return map;
    }

    public void loadStrategy(StrategyChoiceAgent strategyChoice) {
        switch (strategyChoice) {
            case RANDOM:
                this.strategy = new StrategyRandom(this);
                break;
            case USER:
                this.strategy = new StrategyUser(this);
                break;
            case USER_NETWORK:
                this.strategy = new StrategyUserNetwork(this);
                break;
            case AI_TARGET:
                this.strategy = new StrategyAITarget(this);
                break;
            default:
                throw new IllegalArgumentException("This strategy doesn't exist");
        }
    }

    public void algo(BombermanGame game) {
        this.strategy.algo(game);
    }

    public StrategyChoiceAgent getStrategyChoiceAgent() {
        return strategyChoiceAgent;
    }
}