package Game.Agent;

import Game.Info.AgentAction;
import Game.Info.ColorAgent;
import Game.Map;
import Game.Strategy.StrategyChoiceAgent;

public class AgentBomberman extends Agent {
    private int nbBombs = 10;
    private int range = 2;
    private int playerNumber = 1;
    private int score = 0;

    public AgentBomberman(int x, int y, AgentAction agentAction, ColorAgent color, boolean isInvincible, boolean isSick, StrategyChoiceAgent choice, int playerNumber) {
        super(x, y, agentAction, 'B', color, isInvincible, isSick, choice);
        this.playerNumber = playerNumber;
    }

    @Override
    public boolean canPutBomb(Map map) {
        return (nbBombs > 0 && !isSick());
    }

    @Override
    public Map putBomb(Map map) {
        super.setAgentAction(AgentAction.PUT_BOMB);
        return map;
    }

    public int getRange() {
        return range;
    }
    public int getNbBombs() {
        return nbBombs;
    }
    public int getPlayerNumber() {
        return playerNumber;
    }
    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }
    public void setRangeUp() {
        this.range++;
    }
    public void setRangeDown() {    if(this.range>0) this.range--;    }
    public void setNbBombsUp() {
        this.nbBombs++;
    }
    public void setNbBombsDown() {   if(this.nbBombs>0) this.nbBombs--;    }

    public void rmBomb() {
        this.nbBombs--;
    }
}
