package Game.Agent;

import Game.Info.AgentAction;
import Game.Info.ColorAgent;
import Game.Strategy.StrategyChoiceAgent;

public class AgentEnnemi extends Agent {
    public AgentEnnemi(int x, int y, AgentAction agentAction, ColorAgent color, boolean isInvincible, boolean isSick, StrategyChoiceAgent choice) {
        super(x, y, agentAction, 'E', color, isInvincible, isSick, choice);
    }
}
