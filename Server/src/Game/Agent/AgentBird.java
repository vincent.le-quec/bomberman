package Game.Agent;

import Game.Info.AgentAction;
import Game.Info.ColorAgent;
import Game.Map;
import Game.Strategy.StrategyChoiceAgent;

/**
 * L'agent Bird peut sauter par dessus les murs
 * Ici la règle est mise sur 1 mur max à franchir
 */
public class AgentBird extends Agent {

    private int range = 2;

    public AgentBird(int x, int y, AgentAction agentAction, ColorAgent color, boolean isInvincible, boolean isSick, StrategyChoiceAgent choice) {
        super(x, y, agentAction, 'V', color, isInvincible, isSick, choice);
    }

    private boolean isWall(AgentAction action, Map map, int radius) {
        int x = this.getX();
        int y = this.getY();

        switch (action) {
            case MOVE_UP:
                return (y - radius >= 0) && (map.get_walls()[x][y - radius] || map.getStart_brokable_walls()[x][y - radius]);
            case MOVE_DOWN:
                return (y + radius < map.getSizeY()) && (map.get_walls()[x][y + radius] || map.getStart_brokable_walls()[x][y + radius]);
            case MOVE_LEFT:
                return (x - radius >= 0) && (map.get_walls()[x - radius][y] || map.getStart_brokable_walls()[x - radius][y]);
            case MOVE_RIGHT:
                return (x + radius < map.getSizeX()) && (map.get_walls()[x + radius][y] || map.getStart_brokable_walls()[x + radius][y]);
            default:
                return false;
        }
    }

    private int checkCoordUp(Map map) {
        int y = this.getY();

        if (isWall(AgentAction.MOVE_UP, map, 1))
            if (isWall(AgentAction.MOVE_UP, map, 2))
                return -1;
            else
                y -= 2;
        else
            y -= 1;
        return y;
    }
    private int checkCoordDown(Map map) {
        int y = this.getY();

        if (isWall(AgentAction.MOVE_DOWN, map, 1))
            if (isWall(AgentAction.MOVE_DOWN, map, 2))
                return -1;
            else
                y += 2;
        else
            y += 1;
        return y;
    }
    private int checkCoordLeft(Map map) {
        int x = this.getX();

        if (isWall(AgentAction.MOVE_LEFT, map, 1))
            if (isWall(AgentAction.MOVE_LEFT, map, 2))
                return -1;
            else
                x -= 2;
        else
            x -= 1;
        return x;
    }
    private int checkCoordRight(Map map) {
        int x = this.getX();
        if (isWall(AgentAction.MOVE_RIGHT, map, 1))
            if (isWall(AgentAction.MOVE_RIGHT, map, 2))
                return -1;
            else
                x += 2;
        else
            x += 1;
        return x;
    }

    @Override
    public boolean canMoveUp(Map map) {
        int x = this.getX();
        int y;
        if ((y = this.checkCoordUp(map)) == -1)
            return false;
        return super.checkLimits(map, x, y);
    }
    @Override
    public boolean canMoveDown(Map map) {
        int x = this.getX();
        int y;
        if ((y = this.checkCoordDown(map)) == -1)
            return false;
        return super.checkLimits(map, x, y);
    }
    @Override
    public boolean canMoveLeft(Map map) {
        int x;
        int y = this.getY();

        if ((x = this.checkCoordLeft(map)) == -1)
            return false;
        return super.checkLimits(map, x, y);
    }
    @Override
    public boolean canMoveRight(Map map) {
        int x;
        int y = this.getY();

        if ((x = this.checkCoordRight(map)) == -1)
            return false;
        return super.checkLimits(map, x, y);
    }

    @Override
    public Map moveUp(Map map) {
        int id = super.findIdAgent(map);

        if (id != -1) {
            int y;
            if ((y = this.checkCoordUp(map)) == -1) {
                // TODO: renvoyer une erreur
                super.setAgentAction(AgentAction.STOP);
                return map;
            }
            map.setAgentPosition(id, this.getX(), y);
            this.setPosition(this.getX(), y);
            super.setAgentAction(AgentAction.MOVE_UP);
        }
        return map;
    }
    @Override
    public Map moveDown(Map map) {
        int id = super.findIdAgent(map);

        if (id != -1) {
            int y;
            if ((y = this.checkCoordDown(map)) == -1) {
                // TODO: renvoyer une erreur
                super.setAgentAction(AgentAction.STOP);
                return map;
            }
            map.setAgentPosition(id, this.getX(), y);
            this.setPosition(this.getX(), y);
            super.setAgentAction(AgentAction.MOVE_DOWN);
        }
        return map;
    }
    @Override
    public Map moveLeft(Map map) {
        int id = super.findIdAgent(map);

        if (id != -1) {
            int x;
            if ((x = this.checkCoordLeft(map)) == -1) {
                // TODO: renvoyer une erreur
                super.setAgentAction(AgentAction.STOP);
                return map;
            }
            map.setAgentPosition(id, x, this.getY());
            this.setPosition(x, this.getY());
            super.setAgentAction(AgentAction.MOVE_LEFT);
        }
        return map;
    }
    @Override
    public Map moveRight(Map map) {
        int id = super.findIdAgent(map);

        if (id != -1) {
            int x;
            if ((x = this.checkCoordRight(map)) == -1) {
                // TODO: renvoyer une erreur
                super.setAgentAction(AgentAction.STOP);
                return map;
            }
            map.setAgentPosition(id, x, this.getY());
            this.setPosition(x, this.getY());
            super.setAgentAction(AgentAction.MOVE_RIGHT);
        }
        return map;
    }

    public int getRange() {
        return range;
    }
}
