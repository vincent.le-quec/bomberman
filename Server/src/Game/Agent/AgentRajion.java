package Game.Agent;

import Game.Info.AgentAction;
import Game.Info.ColorAgent;
import Game.Strategy.StrategyChoiceAgent;

public class AgentRajion extends Agent {
    public AgentRajion(int x, int y, AgentAction agentAction, ColorAgent color, boolean isInvincible, boolean isSick, StrategyChoiceAgent choice) {
        super(x, y, agentAction, 'R', color, isInvincible, isSick, choice);
    }
}
